/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  TextInput,
  View,
  SafeAreaView
} from 'react-native';


import RouterDirection from './src/router';
import ChatScreen from './src/screens/tabs/ChatScreen'
import NewMessageChanged from './src/screens/NewMessageChanged'
import NewBottomFooter from './src/screens/NewBottomFooter'


export default class App extends Component {
  render() {
    return (
      <SafeAreaView style={styles.safeView}>
      <RouterDirection />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  safeView:{
    flex:1,
  }

})
