import {Scene, Router} from 'react-native-router-flux';
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  TextInput,
  View
} from 'react-native';
import { StackNavigator } from 'react-navigation';


import Signup from '../screens/Signup/';
import Splash from '../screens/Splash/';
import ForgetScreen from '../screens/ForgetScreen/';
import FirstScreen from '../screens/FirstScreen/';
import CreateAccountTwo from '../screens/CreateAccountTwo/';
import NewMessageChanged from '../screens/NewMessageChanged/'
import NewMessage from '../screens/NewMessage/'
import HomeScreen from '../screens/tabs/HomeScreen';
import ChatScreen from '../screens/tabs/ChatScreen';
import NewBottomFooter from '../screens/NewBottomFooter'
import MessageScreen from '../screens/tabs/MessageScreen'

class RouterDirection extends Component{
constructor(props){
  super(props);
  this.state={
    isLoading:true,
  };
}
componentWillMount(){
  setTimeout(()=>{
    this.setState({isLoading:false})
  },2000)
}
_renderScenes(){
  return(
    <Scene key="root">


      <Scene
        key="firstScreen" component={FirstScreen}	panHandlers={null} hideNavBar hideTabBar initial headerMode='none' />


      <Scene
        key="createAccountTwo" component={CreateAccountTwo}	panHandlers={null} hideNavBar hideTabBar headerMode='none' />


      <Scene
        key="signup" component={Signup} panHandlers={null} hideNavBar hideTabBar headerMode='none' />

      <Scene
        key="forgetScreen" component={ForgetScreen}	panHandlers={null} hideNavBar hideTabBar headerMode='none' />

      <Scene
          key="newBottomFooter" component={NewBottomFooter}	panHandlers={null} hideNavBar hideTabBar headerMode='none' />

      <Scene
          key="homeScreen" component={HomeScreen}	panHandlers={null} hideNavBar hideTabBar headerMode='none' />


      <Scene
          key="messageScreen" component={MessageScreen}	panHandlers={null} hideNavBar hideTabBar headerMode='none' />

      <Scene
          key="newMessageChanged" component={NewMessageChanged}	panHandlers={null} hideNavBar hideTabBar headerMode='none' />

      <Scene
          key="newMessage" component={NewMessage}	panHandlers={null} hideNavBar hideTabBar headerMode='none' />

      <Scene
          key="chatScreen" component={ChatScreen}	panHandlers={null} hideNavBar hideTabBar headerMode='none' />

    </Scene>
  )
}
	render(){
    if(this.state.isLoading===true){
      return(<Splash/>);
    }
    else{
      return(
  			<Router>
  		      {this._renderScenes()}
  			</Router>
  		);
    }

	}
}
export default RouterDirection;
