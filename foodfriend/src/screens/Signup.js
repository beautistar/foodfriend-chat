import { Actions } from 'react-native-router-flux';
import React, { Component } from 'react';
import { Header, Left, Body, Right,InputGroup,Icon,Input,Content} from 'native-base';
import {
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  TextInput,
  KeyboardAvoidingView,
  Keyboard,
  View
} from 'react-native';

 class Signup extends Component<{}> {

    constructor(props){
      super(props);

      this.state={
        visited:false,
        email:'',
        emailValid:true,
        passValid:true,
        pass:''
      }
    }

    validate() {

      let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
      if(this.state.email=='')
      {
          //alert('Youe must Enter The Email');
          this.setState({emailValid:false});
          this.refs.email.focus();
      }
      else if(reg.test(this.state.email) === false)
      {

          this.setState({emailValid:false});
          this.refs.email.focus();
          return null;

      }
      else if(this.state.pass==''){

        this.setState({passValid:false});
        this.setState({emailValid:true});
        this.refs.passs.focus();
        return null;

      }
      else {
        //do something like callint another screen
        Keyboard.dismiss();
        this.setState({passValid:true,emailValid:true});
        Actions.newBottomFooter();
        this.setState({visited:true});

      }

    }



    emailErrorVisible (){

      if(this.state.emailValid===false )
      {

          return( <Text style={{color:'red'}}>* Please enter your  email id</Text>)

      }


    }
    passErrorVisible (){

      if(this.state.passValid===false )
      {
          return( <Text style={{color:'red'}}>* Password is required</Text>)

      }

    }

  render() {
  return (


      <View style={styles.container}>

        <View style={styles.navBar}>

                  <Header style={{backgroundColor:'#ffffff'}}>

                    <View style={styles.nav}>
                          <Text onPress={()=>Actions.pop()}>Cancel</Text>
                          <Text style={styles.currentSelected}>Login</Text>
                          <Text style={{color:'#ffffff'}}>Login</Text>
                    </View>
                  </Header>

        </View>

        <View style={styles.contentText}>

          <Text style={styles.item1}>
            Welcome back!
          </Text>

          <Text style={styles.item2}>
            Login with your email to get access in your
            own space in restaurant business
          </Text>

        </View>

        <View style={styles.inputFields}>

          <TextInput
                    ref='email'
                    placeholder='Email'
                    returnKeyType='next'
                    autoCapitalize='none'
                    keyboardType='email-address'
                    style={styles.fields}

                    onChangeText={(text) => this.setState({email:text})}
                    />
          {this.emailErrorVisible()}

          <TextInput
                    ref='passs'
                    placeholder='Password'
                    secureTextEntry
                    style={styles.fields}
                    onChangeText={(text) => this.setState({pass:text})}

                    />
          {this.passErrorVisible()}
        </View>

        <View style={styles.contentText}>

           <TouchableOpacity style={styles.forgetField}>
                <Text style={styles.forgetText} onPress={()=> Actions.forgetScreen()}>Forget Password?</Text>
            </TouchableOpacity>


        </View>

         <KeyboardAvoidingView behavior= {Platform.OS === 'ios'?'padding':null} keyboardVerticalOffset={Platform.OS === 'ios' ?0:0}>
          <TouchableOpacity style={styles.button}
                            onPress={()=> this.validate()}>
            <Text style={styles.buttonText }>Login</Text>
          </TouchableOpacity>
        </KeyboardAvoidingView>

      </View>



    );
  }
}

const styles = StyleSheet.create({

  container: {
    backgroundColor: '#ffffff',
    flex:1,

  },
  nav:{
    flex:1,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'space-between'

  },
  button:{
    
    padding:10,
    backgroundColor:'#0086f8',
    alignItems:'center',

  },

  buttonText:{
    color:'#ffffff'
  },
  contentText:{
    flex:6,
    backgroundColor: '#ffffff',
    paddingTop:10,
    alignItems:'center',
    justifyContent:'center'
  },
  item1:{
    fontSize:18,
    fontWeight:'bold',
    color:'#0086f8'
  },
  item2:{
    fontSize:12,
    marginTop:5,
    justifyContent:'center',
    width:250,
    textAlign:'center',
    color:'black'
  },
  inputFields:{
    padding:10
  },
  forgetField:{
    flex:2,
  },
  forgetText:{
    color:'#0086f8',
    fontSize:15
  },
  currentSelected:
  {

    fontWeight:'bold',
    fontSize:15,
    color:'#0086f8'
  },
  fields:{
    borderBottomWidth:Platform.OS === 'ios'?0.5:0,
    borderBottomColor:Platform.OS === 'ios'?'#d3d3d3':'transparent',
    marginTop:Platform.OS === 'ios'?'10%':0
  },
});
export default Signup;
