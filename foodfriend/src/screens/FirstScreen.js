import { Actions } from 'react-native-router-flux';
import React, { Component } from 'react';
import { Footer ,Button} from 'native-base';
import {
  Platform,StyleSheet,Text,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  View,Animated,LayoutAnimation,NativeModules
} from 'react-native';
var dim= Dimensions.get('window');
const { UIManager } = NativeModules;

UIManager.setLayoutAnimationEnabledExperimental &&
  UIManager.setLayoutAnimationEnabledExperimental(true);

class FirstScreen extends Component{
  constructor(props){
    super(props);
    this.state = {
      ma:dim.height+100
    }

  }
  componentDidMount(){
    LayoutAnimation.spring();
    this.setState({ma:0})


  }
	render(){

    const marginAnimation = {marginTop:this.state.ma}

    return(

  		<ImageBackground style={[styles.container,marginAnimation]}
					source={require('../assests/images/login.png')}>

          <View style={styles.footerField}>

           <TouchableOpacity style={styles.createBtn}
                    onPress={()=>Actions.createAccountTwo()}>
                    <Text style={styles.createBtnStyle}>Create Account</Text>
           </TouchableOpacity>
           <TouchableOpacity style={styles.logBtn}
                    onPress={()=>Actions.signup()}>
                    <Text style={styles.logBtnStyle}>Login</Text>
           </TouchableOpacity>

          </View>
			</ImageBackground>
		);
	}
}

const styles = StyleSheet.create({

	container:{
    flex:1,
		//height:Platform.OS ==='ios'?0:dim.height-70,
		width:dim.width,
		//position:'absolute'

	},
	btnContainer:{
    height:70,
    flexDirection:'row',
    backgroundColor:'red'
  },

  createBtn:{
    height:65,
    flex:1,
    justifyContent:'center',
    alignItems:'center',
    width:dim.width/2,
    //padding:10,
    marginRight:Platform.OS ==='ios'?1.4:1,
    backgroundColor:'#rgb(70,118,174)',
    //alignItems:'center',

  },
  logBtn:{
    flex:1,
    justifyContent:'center',
    alignItems:'center',
    height:65,
    backgroundColor:'#rgb(70,118,174)',
    width:dim.width/2,
//    padding:10,


    alignItems:'center',

  },
  createBtnStyle:{
    marginTop:5,
    color:'#ffffff'
  },
  logBtnStyle:{
    marginTop:5,
    color:'#ffffff'
  },
  footerField:{
    flex:1,
    //marginTop:dim.height-75,
    alignItems:'flex-end',
    justifyContent:'center',
    flexDirection:'row',
    //position:'relative',
    //backgroundColor:'#rgba(255,255,255,0.3)'

  },


});
export default FirstScreen;
