import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import {
  Platform,
  Dimensions,
  StyleSheet,
  Text,
  ImageBackground,
  TouchableOpacity,
  TextInput,
  Image,
  View,
  KeyboardAvoidingView,
  ScrollView,
} from 'react-native';
import {Header ,Footer, Left , Body , Right, FooterTab } from 'native-base';

class CreateAccountTwo extends Component{
    constructor(props){
      super(props);

      this.state={

        firstName:'',
        lastName:'',
        email:'',
        restaurantName:'',
        website:'',
        pass:'',
        firstNameValid:true,
        lastNameValid:true,
        emailValid:true,
        restaurantNameValid:true,
        websiteValid:true,
        passValid:true,
      }
    }

    validate = () => {

    let stringCheker = /^[a-zA-Z]+$/;
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;

      if(this.state.firstName==''){

        this.setState({firstNameValid:false});
        this.refs.fname.focus();
        return null;


      }
      else if(stringCheker.test(this.state.firstName)===false){

        this.setState({firstNameValid:false});
        this.refs.fname.focus();
        return null;
      }

      else if(this.state.lastName==''){

        this.setState({lastNameValid:false,firstNameValid:true});
        this.refs.lName.focus();
        return null;
      }

      else if(stringCheker.test(this.state.lastName)===false){
        this.setState({lastNameValid:false,firstNameValid:true});
        this.refs.lName.focus();
        return null;
      }
      else if(this.state.email=='')
      {
          this.setState({emailValid:false,lastNameValid:true,firstNameValid:true});
          this.refs.emailField.focus();
          return null;
      }
      else if(reg.test(this.state.email) === false)
      {

          this.setState({emailValid:false,lastNameValid:true,firstNameValid:true});
          this.refs.emailField.focus();
          return null;
      }


      else if(this.state.restaurantName == '')
      {

          this.setState({emailValid:false,restaurantNameValid:true,emailValid:true,lastNameValid:true,firstNameValid:true});
          this.refs.restaurantName.focus();
          return null;
      }
      else if(this.state.website == '')
      {

          this.setState({websiteValid:false,emailValid:true,restaurantNameValid:true,emailValid:true,lastNameValid:true,firstNameValid:true});
          this.refs.website.focus();
          return null;
      }
      else if(this.state.pass==''){

        this.setState({passValid:false,websiteValid:true,emailValid:true,lastNameValid:true,firstNameValid:true});
        this.refs.pass.focus();
        return null;
      }
    else{
        this.setState({passValid:true,emailValid:true,websiteValid:true,restaurantNameValid:true,lastNameValid:true,firstNameValid:true});
        Actions.pop();
      }

    }

    firstNameErrorVisible (){

      if(this.state.firstNameValid===false )
      {

          return( <Text style={{color:'red',marginLeft:20}}>* First name is require</Text>)

      }


    }
    lastNameErrorVisible (){

      if(this.state.lastNameValid===false )
      {

          return( <Text style={{color:'red'}}>* Last name is required</Text>)

      }

    }

    emailErrorVisible (){

      if(this.state.emailValid===false )
      {

          return( <Text style={{color:'red'}}>* Please enter your email id</Text>)

      }


    }
    restaurantNameErrorVisible (){

      if(this.state.restaurantNameValid===false )
      {

          return( <Text style={{color:'red'}}>* Restaurant name is required</Text>)

      }

    }

    websiteErrorVisible (){

      if(this.state.websiteValid===false )
      {

          return( <Text style={{color:'red'}}>* Website name is require</Text>)

      }


    }
    passErrorVisible (){

      if(this.state.passValid===false )
      {

          return( <Text style={{color:'red'}}>* Password is required</Text>)

      }

    }



  render(){
    return(
      <View style={{flex:1}}>


          <View>
          <Header style={{backgroundColor:'#ffffff'}}>

            <View style={styles.nav}>
                  <Text onPress={()=>Actions.pop()}>Cancel</Text>
                  <Text style={styles.currentSelected}>Create Account</Text>
                  <Text style={{color:'#ffffff'}}>Login</Text>

            </View>
          </Header>


          </View>


           <ScrollView >
              <View style={{padding:10}}>
                        <View style={styles.contents}>

                               <Text style={styles.mainContents}>Welcome to Food Friends Network!</Text>

                        </View>

                        <View style={styles.topField}>


                                <Image style={styles.img}
                                source={require('../assests/images/splash2.png')}
                                />


                              <View>
                              <TextInput
                              ref='fname'
                              onChangeText={(text) => this.setState({firstName:text})}
                              returnKeyType='next'
                              onSubmitEditing={()=>this.refs.lName.focus()}
                              style={styles.fField}
                              placeholder='First Name'/>
                              {this.firstNameErrorVisible()}
                              </View>

                        </View>

                        <View>
                              <TextInput
                              ref='lName'
                              onChangeText={(text) => this.setState({lastName:text})}
                              returnKeyType='next'
                              style={styles.fields}
                              onSubmitEditing={()=>this.refs.emailField.focus()}
                              placeholder='Last Name'/>
                              {this.lastNameErrorVisible()}

                              <TextInput
                              ref='emailField'
                              onChangeText={(text) => this.setState({email:text})}
                              autoCapitalize='none'
                              style={styles.fields}
                              keyboardType='email-address'
                              onSubmitEditing={()=>this.refs.restaurantName.focus()}
                              returnKeyType='next'
                              placeholder='Email'/>
                              {this.emailErrorVisible()}

                              <TextInput
                              ref='restaurantName'
                              onChangeText={(text)=>this.setState({restaurantName:text})}
                              returnKeyType='next'
                              style={styles.fields}
                              onSubmitEditing={()=>this.refs.website.focus()}
                              placeholder='Restaurant Name'/>
                              {this.restaurantNameErrorVisible()}

                              <TextInput
                              ref='website'
                              onChangeText={(text)=>this.setState({website:text})}
                              returnKeyType='next'
                              style={styles.fields}
                              onSubmitEditing={()=>this.refs.pass.focus()}
                              placeholder='Website'/>
                              {this.websiteErrorVisible()}

                              <TextInput
                              ref='pass'
                              onChangeText={(text)=>this.setState({pass:text})}
                              returnKeyType='go'
                              style={styles.fields}
                              secureTextEntry
                              placeholder='Password'/>
                              {this.passErrorVisible()}
                        </View>

                        <View style={styles.conditions}>
                            <Text style={{textAlign:'center',}}>
                            <Text style={{fontSize:12}}>By creating an account and signing up to
                            Food Friends I accept the<Text style={{color:'#0086f8',fontSize:12}}> Tearms of Services</Text><Text style={{fontSize:12}}> and </Text>
                            <Text style={{fontSize:12,color:'#0086f8'}}> Privacy Policy</Text>
                            </Text>
                            </Text>

                        </View>

              </View>

           </ScrollView>



           <KeyboardAvoidingView behavior= {Platform.OS === 'ios'?'padding':null} keyboardVerticalOffset={Platform.OS === 'ios' ?0:0}>
            <View style={styles.footerField}>

             <TouchableOpacity style={styles.button}
                      onPress={()=>this.validate()}>
                      <Text style={styles.buttonText}>Create Account</Text>
                 </TouchableOpacity>

            </View>
          </KeyboardAvoidingView>

      </View>
    );
  }
}
const styles=StyleSheet.create({

  createHeader:{
    fontSize:15,
    color:'black',
    justifyContent:Platform.OS ==='ios'?'center':'center',
    alignItems:'center',
    marginLeft:Platform.OS ==='ios'?0:20,
  },
  container: {

    padding:10,
      backgroundColor: '#ffffff',
      flexDirection:'row',
      justifyContent:'center'
  },

  topButton:{
    justifyContent:'center'

  },
  contents:{

    alignItems:'center',
    justifyContent:'center'
  },
  fField:{
    borderBottomWidth:Platform.OS === 'ios'?0.5:0,
    borderBottomColor:Platform.OS === 'ios'?'#d3d3d3':'transparent',
    marginTop:Platform.OS === 'ios'?30:0,
    marginLeft:20,
    width:Dimensions.get('window').width,
  },
  mainContents:{

    fontSize:18,
    padding:10,
    color:'#0086f8'
  },
  topField:{

    marginTop:5,
    zIndex:10,
    flexDirection:'row',
    overflow:'hidden'

  },
  img:{
    height:Platform.OS === 'ios'?70:60,
    width:Platform.OS === 'ios'?70:60,
    borderRadius:Platform.OS === 'ios'?35:70
  },

  conditions:{
    flex:Platform.OS === 'ios'?1:0,
    alignItems:'center',
    justifyContent:'center',
    margin:10,
    marginTop:Platform.OS === 'ios'?'20%':30

  },

  button:{

    width:Dimensions.get('window').width,
    padding:10,
    backgroundColor:'#0086f8',
    alignItems:'center',

  },
  buttonText:{
    alignItems:'center',

    justifyContent:'center',
    color:'white'
  },
  footerField:{

    marginTop:40,
    flexDirection:'row',
    justifyContent:'center',
    alignItems:'flex-end',
    backgroundColor:'#ffffff'

  },
  //for textinput in ios specific
  fields:{
    borderBottomWidth:Platform.OS === 'ios'?0.5:0,
    borderBottomColor:Platform.OS === 'ios'?'#d3d3d3':'transparent',
    marginTop:Platform.OS === 'ios'?'10%':0
  },
  nav:{
    flex:1,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'space-between'

  },
});
export default CreateAccountTwo;
