
import React, { Component } from 'react';
import {
  Platform,Dimensions,StyleSheet,
  Text,TouchableOpacity,TextInput,
  Image,View,Animated
} from 'react-native';
const splashImage=require('../assests/images/splash2.png');
var dim= Dimensions.get('window')
import Signup from './Signup'
import FirstScreen from './FirstScreen'

 class Splash extends Component<{}> {

  /*constructor(props){
    super(props);
    this.state = {
        //opacity:new Animated.Value(0),
        timePassed: true,
    };
  }*/

/*  componentDidMount() {
        Animated.sequence([
          Animated.timing(this.state.opacity,{
              toValue:1,
              duration:500,
              useNativeDriver: true,
              }),
              Animated.timing(this.state.opacity,{
                  toValue:0,
                  duration:1500,
                  useNativeDriver: true,
              })

        ]).start();

        setTimeout( () => {

            //load splach screen to call below method
            this.setTimePassed();

        },2100);
  }

  setTimePassed() {

      this.setState({timePassed: false});
  }*/

 render() {
      /*  const opacityAnimation={opacity:this.state.opacity}
        if(!this.state.timePassed){
            return (
                    <FirstScreen />
            );
        }
        else{

          return(
                <Animated.View style={[styles.container.opacityAnimation]}>
                  <Image style={styles.img}
            					source={require('../assests/images/splash2.png')} />
                </Animated.View>
          );
        }
      }*/

      return(
        <View style={styles.container}>
          <Image style={styles.img}
              source={splashImage} />
        </View>
      )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
//    backgroundColor: '#F5FCFF',
  //  position: 'relative',

  },
  img:{
    height:dim.height,
    width:dim.width,
    //resizeMode:Platform.OS ==='ios'?'contain':'stretch'
  },
});
export default Splash;
