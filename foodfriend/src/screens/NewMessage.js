import {Actions} from 'react-native-router-flux'
  import type {
  StyleObj,
} from 'react-native/Libraries/StyleSheet/StyleSheetTypes';

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  FlatList,
  Dimensions,
  ScrollView,
  TouchableOpacity,
  KeyboardAvoidingView,
  TextInput,
  ImageBackground,
  Image,
  View,
  Keyboard,
  Animated,
  ListView,TouchableWithoutFeedback

} from 'react-native';
//library for chip/tags
import TagInput from '../lib/TagInput';


import {TabNavigator} from 'react-navigation';
import {CheckBox, Container, Header, Tab,Icon, Tabs , ScrollableTab,FooterTab,Footer} from 'native-base';
import Restaurants from './messagetabs/Restaurants/'

import checkBoxListData from '../demodata/checkBoxListData'

var selectedUser=[];
const rec=1;
class NewMessage extends Component{

  constructor(props){
    super(props);
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
    //this.refs.search.focus();

    this.state={
        tags: [],
        text: "",
        text1:'',
        to:'',
        newData:checkBoxListData,
        dataSource:ds.cloneWithRows(checkBoxListData),
        sliderAnimation:new Animated.Value(0),
        animSearchBox:new Animated.Value(-10),
        dynamicStyle:-200,
        mHeight:510

    }

  }

  _onBlur(){
  Animated.timing(this.state.animSearchBox,{
     toValue:-10,
     uration:200
   }).start()
  }

  _animSearch(){
   this.refs.searchBox.focus();
  Animated.timing(this.state.animSearchBox,{
               toValue:Dimensions.get('window').width-150,
               duration:200
   }).start()
  }
  componentWillMount(){
  //do it later
    //this.state.newData.map()

  }
  __visibleGroup(){

      const animationStyle={height:this.state.sliderAnimation}
      return(
            <Animated.View style={[styles.firstCon,animationStyle]}>
            <Image style={styles.imgBox} source={require('../assests/images/camera.png')}/>

            <TextInput
            underlineColorAndroid='transparent'
            style={styles.inputBox}
            placeholder='Group Name'/>

            <TouchableOpacity
            style={styles.btnStyle} onPress={()=>Actions.chatScreen()}>
                  <Text style={styles.btnTextStyle}>Save</Text>
            </TouchableOpacity>
            </Animated.View>
          );


  }
  //finding the index of ckacked
  _utilityToggleCheckBox(text){
      let {newData} = this.state;
      for(var i=0;i<checkBoxListData.length;i++){
        if(newData[i].name == text){
          return i
        }
      }
      return -1
  }

  componentDidMount(){
    Keyboard.dismiss();
  }

  //for handling checkbox events
  checkHandler(name){
      //getting the position of that check/uncheckec row
      var findchecked = this._utilityToggleCheckBox(name)

      //update the new data
      var newDataSource=this.state.newData
      newDataSource[findchecked].isChacked=!newDataSource[findchecked].isChacked
      //(newDataSource[findchecked].isChacked)
    //  console.log(newDataSource);

      var newDataSourceRow = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})

      //update the the old dataSource to new dataSource29-JAN IN @9:10AM
      this.setState({
          newData:newDataSource,
          dataSource:newDataSourceRow.cloneWithRows(newDataSource)
      })
      //console.log(this.state.dataSource);
      //array for to field
      if(selectedUser.indexOf(name) >= 0){
          var del=selectedUser.indexOf(name)
          if (del > -1) {
              selectedUser.splice(del, 1);
              this.setState({
                tags:selectedUser
              })

          }

      }else{
        selectedUser.push(name)
        this.setState({
          tags:selectedUser
        })

      }
      // animation for group box
      if(selectedUser.length >=2){
        Animated.timing(this.state.sliderAnimation,{
          toValue:70,
          duration:500
        }).start()
        this.setState({dynamicStyle:-220})
      }else{
        Animated.timing(this.state.sliderAnimation,{
          toValue:0,
          duration:500
        }).start()
        this.setState({dynamicStyle:-150})
      }
      this.setState({
          to:selectedUser.toString()
      })
      this.refs.scrollview.scrollToEnd()

  }
  //for footer tab
  footer(){
    const marginTopDynamic={marginTop:this.state.dynamicStyle}
    return(

      <Footer style={styles.footerTabs}>
         <FooterTab style={{backgroundColor:'#ffffff'}}>
           <View style={{width:'70%'}}>
           <TextInput
                 underlineColorAndroid='transparent'
                 placeholder='Type your message here'
                 ref='typeBox'
                 />

           </View>
           <View style={styles.sendBoxContainer}>
               <TouchableOpacity><Text style={styles.btnSend}>Send</Text></TouchableOpacity>
           </View>
         </FooterTab>
      </Footer>

    )
  }

  renderRow(rowData){

        return(

          <View style={styles.singleContainer}>

                <View style={styles.img}>
                      <Image style={styles.imgStyle}
                      source={require('../assests/images/user3.png')}
                      />
                </View>
                <View style={styles.Profile}>
                      <Text style={styles.pName}>{rowData.name}</Text>
                      <Text style={styles.msg}>{rowData.msg}</Text>
                </View>
                <View style={styles.timeHere}>
                  <CheckBox checked={rowData.isChacked} onPress={()=> this.checkHandler(rowData.name)} style={styles.checkboxStyle}/>
                </View>

          </View>
        )
  }


  // For updatting the dataSource by searching
  filterSearch(text){
       const newData = checkBoxListData.filter(function(item){
           const itemData = item.name.toUpperCase()
           const textData = text.toUpperCase()
           return itemData.indexOf(textData) > -1
       })
       this.setState({
           dataSource: this.state.dataSource.cloneWithRows(newData),
           text1: text
       })
   }

//when chip is added new
    onChangeText = (text) => {
      this.setState({ text });

      const lastTyped = text.charAt(text.length - 1);
      const parseWhen = [',', ' ', ';', '\n'];

      if (parseWhen.indexOf(lastTyped) > -1) {
        this.setState({
          tags: [...this.state.tags, this.state.text],
          text: "",
        });
      }
    }
    //scroll to end while keyboard appears

    //when chip is being to deleting
    onChangeTags = (tags) => {

      selectedUser=tags

      var newDataSourceTag=this.state.newData

      //for unchecked checkbox via tags
      for (var i=0;i<newDataSourceTag.length;i++){
                newDataSourceTag[i].isChacked = false
                for(var j=0;j<selectedUser.length;j++){

                  if(newDataSourceTag[i].name == selectedUser[j]){
                    newDataSourceTag[i].isChacked = true
                  }
                }
      }
      var newDataSourceRowTag = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})

      //update the the old dataSource to new dataSource
      this.setState({
          newData:newDataSourceTag,
          dataSource:newDataSourceRowTag.cloneWithRows(newDataSourceTag),
          tags:selectedUser
      })

      if(selectedUser.length >=2){
        Animated.timing(this.state.sliderAnimation,{
          toValue:Platform.OS ==='ios'?50:70,
          duration:500
        }).start()
        this.setState({dynamicStyle:-220})
      }else{
        Animated.timing(this.state.sliderAnimation,{
          toValue:0,
          duration:500
        }).start()
        this.setState({dynamicStyle:-150})
      }
      this.setState({
          to:selectedUser.toString()
      })


    }

  render(){
    const animSearch = {paddingRight:this.state.animSearchBox}
    const inputProps = {
         keyboardType: 'default',
         placeholder: '',
         autoFocus: true,
         style: {
           fontSize: 14,
           marginVertical: Platform.OS == 'ios' ? 10 : -2,
         },
       };
    return(

      <View  style={styles.container} >

            <ImageBackground style={styles.topBackground}
                source={require('../assests/images/topBar.png')} >
                  <View>
                        <TouchableOpacity onPress={()=> Actions.pop()} style={{height:40,width:60,}}>
                          <Icon name='arrow-back' style={{color:'#ffffff',fontSize:25,}}/>
                        </TouchableOpacity>
                  </View>
                  <View style={styles.menuContainer}>

                            <Text style={styles.privateMessageStyle}>New Message</Text>

                  </View>
                  <View>

                  </View>


            </ImageBackground>

            {this.__visibleGroup()}

            <View style={styles.secondCon}>
                  <Text style={{color:'#a5b0b9'}}>To: </Text>
                  <ScrollView horizontal={true} vertical={false}
                              ref='scrollview'
                              automaticallyAdjustContentInsets={false}
                    >
                  <View style={{flexDirection:'row'}}>
                    <TagInput

                            value={this.state.tags}
                            onChange={this.onChangeTags}
                            labelExtractor={(tag)=>tag}
                            text={this.state.text}
                            onChangeText={this.onChangeText}
                            tagColor="white"
                            tagTextColor="#0086f8"
                            inputProps={inputProps}
                            maxHeight={75}
                          />

                  </View>
                  </ScrollView>
            </View>


            <View style={styles.thirdCon} keyboardShouldPersistTabs='always'
                             keyboardDismissMode='on-drag'>

                   <Tabs  renderTabBar={()=> <ScrollableTab />} tabBarUnderlineStyle={{ backgroundColor:'#0086f8', }} >

                     <Tab heading="ALL" tabStyle={styles.tabBarBackColor} textStyle={styles.tabTextStyle} activeTabStyle={styles.activeTabStyle} activeTextStyle={styles.activeTextStyle}>

                        <View style={{marginBottom:0,backgroundColor:'#e4e7f4'}}>
                          <TouchableWithoutFeedback style={styles.searchBox} onPress={()=>this._animSearch()}>
                                <Animated.View style={[styles.searchBox,animSearch,{justifyContent:'center'},]}>
                                    <View>
                                          <Icon name='search' size={18} style={{color:'#a1adbc',fontSize:25,}}/>
                                    </View>
                                    <View>
                                          <TextInput
                                            ref='searchBox'
                                            underlineColorAndroid='transparent'
                                            placeholder='Search'
                                            onFocus={()=>this._animSearch()}
                                            onBlur={()=>this._onBlur()}
                                            placeholderTextColor={'#a1adbc'}
                                            onChangeText={(text)=>this.filterSearch(text)}
                                            style={styles.textSearchStyle}/>
                                    </View>
                                </Animated.View>
                          </TouchableWithoutFeedback>

                           <ListView
                             style={[styles.scrollViewStyle]}
                             enableEmptySections={true}

                             renderRow={this.renderRow.bind(this)}
                             dataSource={this.state.dataSource}
                            />

                       </View>

                     </Tab>

                     <Tab heading="RESTAURANT" tabStyle={styles.tabBarBackColor} textStyle={styles.tabTextStyle} activeTabStyle={styles.activeTabStyle} activeTextStyle={styles.activeTextStyle}>
                       <Restaurants />
                     </Tab>

                     <Tab heading="TYPES" tabStyle={styles.tabBarBackColor} textStyle={styles.tabTextStyle} activeTabStyle={styles.activeTabStyle} activeTextStyle={styles.activeTextStyle}>

                     </Tab>

                     <Tab heading="GROUPS" tabStyle={styles.tabBarBackColor} textStyle={styles.tabTextStyle} activeTabStyle={styles.activeTabStyle} activeTextStyle={styles.activeTextStyle}>

                     </Tab>

                   </Tabs>
           </View>
           {/* <KeyboardAvoidingView  behavior= {Platform.OS === 'ios'?'padding':null} keyboardVerticalOffset={Platform.OS === 'ios' ?'45':0}>
             {this.footer()}
           </KeyboardAvoidingView> */}
      </View>

    );
  }
}
const styles=StyleSheet.create({
  topBackground:{

    height:50,
    paddingTop:Platform.OS === 'ios'?20:10,
    backgroundColor:'#0880f3',
    flexDirection:'row',
    justifyContent:'space-between',
    padding:10,
  },
  privateMessageStyle:{
    fontSize:15,
    marginTop:5,
    marginRight:'10%',
    color:'#ffffff'
  },
  footerTabs:{

    //flex:Platform.OS ==='ios'?.023:.12,
    height:Platform.OS ==='ios'?60:60,
    //alignItems:'center',

    padding:Platform.OS === 'ios'?10:5,
    paddingRight:Platform.OS === 'ios'?5:15,
    //justifyContent:'flex-end',
    backgroundColor:'#ffffff'
  },
  btnSend:{
    color:'#2d87d9'
  },
  container:{
    flex:1,

    backgroundColor:'#ffffff',

    //justifyContent:'space-between',
  },
  firstCon:{
    zIndex:-1,
    justifyContent:'center',
    alignItems:'center',
    flexDirection:'row',
    borderBottomWidth:0.3,
    borderColor:'#ffffff',

  },
  imgBox:{
    backgroundColor:'#ffffff',
    height:50,
    width:50,
  },
  inputBox:{
    paddingLeft:5,
    borderWidth:0,
    backgroundColor:'#ffffff',
    width:'60%',
    height:40,
  },
  btnStyle:{
    justifyContent:'center',
    alignItems:'center',
    backgroundColor:'#2d87d9',
    width:'20%',
    height:35,
    padding:10,
  },
  btnTextStyle:{
    color:'#ffffff',
  },
  secondCon:{
    height:Platform.OS ==='ios'?40:40,
    backgroundColor:'#ffffff',
    flexDirection:'row',
    alignItems:'center',
    paddingHorizontal:10,
    borderBottomWidth:0.3,
    borderColor:'black',
  },
  toTextStyle:{
    color:'#2d87d9',
    fontWeight:'bold',
  },
  thirdCon:{
   // height:Platform.OS ==='ios'?735:570,
    flex:1,
    borderBottomWidth:1,
    borderColor:'black',
    backgroundColor:'#ffffff',

  },
  tabBarBackColor:{
    backgroundColor:'#ffffff',

  },
  activeTabStyle:{
    backgroundColor:'#ffffff'
  },
  activeTextStyle:{
    color: 'black',
    fontSize:15,
  },
  tabTextStyle:{
    color:'#b1b5be',
    fontSize:15,
  },
  searchBox:{
    paddingLeft:10,
    margin:10,
    backgroundColor:'#ffffff',
    borderRadius:5,
    height:30,
    alignItems:'center',
    flexDirection:'row',
  },
  viewText:{
    flex:3,
    justifyContent:'center',
    alignItems:'flex-start',
    height:30,
    width:50
  },
  textSearchStyle:{
    justifyContent:'center',
    alignItems:'center',
    fontSize:14,
    width:100,
    alignItems:'center',
    height:40,
  },
  scrollViewStyle:{

    backgroundColor:'#ffffff',

   //maxHeight:Platform.OS ==='ios'?500:340,
    marginBottom:50,

  },
  mainView:{
    backgroundColor:'#e4e7f4',

  },
  singleContainer:{
    backgroundColor:'#ffffff',
    padding:10,
    borderBottomWidth:0.5,
    borderBottomColor:'#eee',
    flexDirection:'row',

  },
  img:{
    padding:5,
    borderRadius:100,
    justifyContent:'center',
  },
  imgStyle:{
    width:Platform.OS === 'ios'?50:40,
    height:Platform.OS === 'ios'?50:40,
    borderRadius:Platform.OS === 'ios'?25:50
  },
  Profile:{
    width:'70%',
    padding:5,
  },
  pName:{

    fontSize:14,
    fontWeight:'bold'
  },
  timeIcon:{
    fontSize:12
  },
  checkboxStyle:{
    height:25,
    marginTop:Platform.OS === 'ios'?'1%':10,
    width:25,
    justifyContent:'center',
    alignItems:'center',
    paddingTop:Platform.OS === 'ios'?'20%':10,
  },
  sendBoxContainer:{
    justifyContent:Platform.OS === 'ios' ?null:'center',
    marginRight:Platform.OS === 'ios' ?'5%':null
  },
  timeHere:{
    justifyContent:'center',
    alignItems:'center',
    marginLeft:Platform.OS === 'ios'?0:'5%'
  },
  msg:{
    color:'#a1adbc',
    fontSize:13,
  },
})
export default NewMessage;
