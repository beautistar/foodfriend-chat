import { Actions } from 'react-native-router-flux';
import React, { Component } from 'react';
import { Header, Left, Body, Right} from 'native-base';
import {
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  Keyboard,
  TextInput,
  View
} from 'react-native';

 class ForgetScreen extends Component<{}> {
  constructor(props){
      super(props);

      this.state={


        email:'',
        emailValid:true,
        emailSent:false

      }
    }

    validate = () => {


    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;

      if(this.state.email==''){

        this.setState({emailValid:false,emailSent:false});

        this.refs.emailField.focus();
        return null;
      }
      else if(reg.test(this.state.email)===false){

          this.setState({emailValid:false,emailSent:false});
          return null;

      }else{
        //Do somethings.......change Screen
          this.setState({emailValid:true,emailSent:true});
           Keyboard.dismiss();
          Actions.firstScreen();
      }

    }

    emailErrorVisible (){

      if(this.state.emailValid===false )
      {

          return( <Text style={{color:'red'}}>* Please enter your email id</Text>)

      }
      if(this.state.emailSent===true)
      {
        return(<Text style={{color:'black',justifyContent:'center',alignItems:'center',fontSize:14}}>* Please check your Inbox</Text>)
      }
    }

  render() {
    return (
      <View style={styles.container}>

        <View style={styles.navBar}>

                  <Header style={{backgroundColor:'#ffffff'}}>

                    <View style={styles.nav}>
                          <TouchableOpacity onPress={()=>Actions.pop()}>
                            <Text>Cancel</Text>
                          </TouchableOpacity>
                          <Text style={styles.currentSelected}>Forget Password</Text>
                          <TouchableOpacity onPress={()=>Actions.pop()}>
                              <Text>Login</Text>
                          </TouchableOpacity>
                    </View>

                  </Header>

        </View>

        <View style={styles.contentText}>

          <Text style={styles.item1}>
           Welcome back!
          </Text>

          <Text style={styles.item2}>
            Enter your email address bellow
          </Text>

        </View>

        <View style={styles.inputFields}>

          <TextInput
                    ref='emailField'
                    onChangeText={(text) => this.setState({email:text})}
                    autoCapitalize='none'
                    style={styles.fields}
                    keyboardType='email-address'
                    placeholder='Email'/>

                    {this.emailErrorVisible()}

        </View>






        <TouchableOpacity style={styles.button}
                            onPress={()=>this.validate()}>
          <Text style={styles.buttonText}>Send</Text>
        </TouchableOpacity>


      </View>



    );
  }
}

const styles = StyleSheet.create({

  container: {
    backgroundColor: '#ffffff',
    flex:1,

  },
  nav:{
    flex:1,
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems:'center'
  },
  button:{

    marginTop:10,
    padding:10,
    backgroundColor:'#0086f8',
    alignItems:'center',

  },

  buttonText:{
    color:'#ffffff'
  },
  contentText:{
    flex:1,
    backgroundColor: '#ffffff',
    paddingTop:10,
    alignItems:'center',
    justifyContent:'center'
  },
  item1:{
    fontSize:18,
    fontWeight:'bold',
    color:'#0086f8'
  },
  item2:{
    fontSize:12,
    marginTop:5,
    justifyContent:'center',
    width:250,
    textAlign:'center',
    color:'black'
  },
  inputFields:{
    padding:10,
    flex:1,
  },
  fields:{
    borderBottomWidth:Platform.OS === 'ios'?0.5:0,
    borderBottomColor:Platform.OS === 'ios'?'#d3d3d3':'transparent',
    marginTop:Platform.OS === 'ios'?25:0
  },
  currentSelected:
  {

    fontWeight:'bold',
    fontSize:15,
    color:'#0086f8'
  },
});
export default ForgetScreen;
