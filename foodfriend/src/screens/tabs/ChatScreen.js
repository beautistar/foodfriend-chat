import { Actions } from 'react-native-router-flux'
import React, { Component } from 'react';
import { Footer ,Button,Icon,Drawer,Content} from 'native-base';

import {
  Platform,
  StyleSheet,
  Text,
  ListView,
  Dimensions,
  TextInput,
  Image,
  ImageBackground,
  TouchableHighlight,
  Keyboard,
  TouchableOpacity,
  View,
  KeyboardAvoidingView
} from 'react-native';
import InvertibleScrollView from 'react-native-invertible-scroll-view';


import data from '../../demodata/messageData/'

class ChatScreen extends Component{
  constructor(props,context){
   super(props,context)
     const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
     this._keyboardWillShowListener=Keyboard.addListener('keyboardWillShow',this._keyboardWillShow);
     this.state={
      position:'absolute',
       dataSource:ds.cloneWithRows(data),
       text:''
     }

  }

  _keyboardWillShow(){
    // this.setState({
    //   position:'absolute'
    // })


  }
  _forScroll(){
//    this.refs.chatView.scrollTo({y:this.refs.chatView.getMatrics().contentLength})
      //alert(this.footerY)


  }
  componentWillUpdate(){
    //this.refs.chatView.scrollToEnd({animate:false})


  }
  sendMessage(text){

   }

  renderRow(rowData){

        const image=rowData.img;


        if(rowData.name == 'John'){
            return(
              <View style={{backgroundColor:'#ffffff',height:100}}>

                    <View style={{padding:10,flexDirection:'row',justifyContent:'flex-end'}}>

                          <View style={{marginRight:'1%',}}>
                              <ImageBackground style={{flexDirection:'row',maxWidth:'90%',minWidth:'60%',padding:10,alignItems:'center'}} source={require('../../assests/images/persionReplied.png')}>
                                  <Text style={{}}>{rowData.msg}</Text>
                              </ImageBackground>
                              <View style={{marginRight:10,padding:5,flexDirection:'row'}}>
                                    <Icon name='checkmark' style={{fontSize:15}} />
                                    <Text style={{fontSize:12,marginRight:10}}> : seen {rowData.time} </Text>
                              </View>
                          </View>
                          <Image style={{height:40,width:40}} source={require('../../assests/images/user2.png')}/>
                    </View>


              </View>
            )
          }else{
            return(
              <View style={{backgroundColor:'#ffffff',height:100,}}>

                    <View style={{padding:10,flexDirection:'row'}}>

                          <Image style={{height:40,width:40}} source={require('../../assests/images/user1.png')}/>

                          <View style={{marginLeft:'5%',}}>
                              <ImageBackground style={{flexDirection:'row',maxWidth:'90%',minWidth:'60%',padding:10,paddingLeft:15,alignItems:'center'}} source={require('../../assests/images/persionQue.png')}>

                                  <Text style={{marginLeft:0}}>{rowData.msg}</Text>
                              </ImageBackground>

                          </View>

                    </View>


              </View>

            )
          }

  }
    // Render a fake footer
   renderFooter = () => {
     return (
       <View onLayout={this.onFooterLayout} />
     )
   }


   onFooterLayout = (event) => {
      const layout = event.nativeEvent.layout
      this.footerY = layout.y
     // this.refs.chatView.scrollToEnd()
   }

  render(){
    return(

      <View style={styles.container}  >

          <View style={{flex:1}}>

                <ImageBackground style={styles.topBackground}
                    source={require('../../assests/images/topBar.png')} >
                      <TouchableOpacity onPress={()=>Actions.pop()} style={{width:50,height:40}}>
                            <Icon name='arrow-back' style={{color:'#ffffff'}}/>
                      </TouchableOpacity>
                      <View style={styles.menuContainer}>
                                <Text style={styles.privateMessageStyle}>Chat Name</Text>
                      </View>
                      <View>
                            <Icon name='more' style={{color:'#ffffff'}}/>
                      </View>
                </ImageBackground>



                <View style={styles.chatContainer}
                  scrollEnabled={true}>
                      <ListView

                        ref='chatView'
                        renderScrollComponent={props => <InvertibleScrollView {...props} inverted />}
                        style={styles.mainContainer}
                        keyboardShouldPersistTabs={true}
                        enableEmptySections={true}
                        renderRow={this.renderRow.bind(this)}
                        dataSource={this.state.dataSource}
                      />
                </View>
          </View>
         <KeyboardAvoidingView behavior= {Platform.OS === 'ios'?'padding':null} keyboardVerticalOffset={Platform.OS === 'ios' ?0:0}>
          <View style={styles.footerContainer}>

                <TextInput
                  ref='typeBox'
                  placeholder='Type your message here'
                  underlineColorAndroid='transparent'
                  placeholderTextColor={'#a1adbc'}
                  onChangeText={(text)=>this.setState({text:text})}
                  style={styles.textInputStyle}
                />
                <TouchableOpacity style={styles.btnSend}
                                  onPress={this.sendMessage(this.state.text)}>
                  <Text style={styles.btnTextStyle}>Send</Text>
                </TouchableOpacity>

        </View>
        </KeyboardAvoidingView>

    </View>

    );
  }
}

const styles=StyleSheet.create({

  container:{
    flex:1,
    backgroundColor:'#eee',

  },
  mainContainer:{
    flex:1,
  },
  topBackground:{

    height:50,
    paddingTop:Platform.OS === 'ios'?20:10,
    backgroundColor:'#0880f3',
    flexDirection:'row',
    justifyContent:'space-between',
    padding:10,
  },
  menuContainer:{
    justifyContent:'center',
    alignItems:'center',
    flexDirection:'row'
  },
  privateMessageStyle:{
    fontSize:15,
    color:'#ffffff'
  },

  chatContainer:{
    flex:1,
  },
  footerContainer:{
    borderTopWidth:0.3,
    height:Platform.OS === 'ios' ?60:50,
    backgroundColor:'#ffffff',
    flexDirection:'row',
    paddingHorizontal:15,
  },
  textInputStyle:{
    width:'80%',

  },
  btnSend:{
    alignItems:'center',
    justifyContent:'center',
    marginLeft:35
  },
  btnTextStyle:{
    color:'#007aff',
    fontSize:15,

  },
});



export default ChatScreen;
