
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  Image,
  TouchableOpacity,
  View
} from 'react-native';
import {TabNavigator,StackNavigator} from 'react-navigation';
import ScreenOne from './ScreenOne';
import ScreenTwo from './tabs/ScreenTwo';
import ScreenThree from './tabs/ScreenThree';
import ScreenFour from './tabs/ScreenFour';
import ScreenFive from './tabs/ScreenFive';
import HomeScreen from './tabs/HomeScreen';


var NavigationTabs = TabNavigator({

  Home:{screen:HomeScreen},
  Notify:{screen:ScreenTwo},
  Cal:{screen:ScreenThree},
  Messages:{screen:ScreenFour},
  Profile:{screen:ScreenFive},

},{
    tabBarPosition: 'bottom',
    tabBarOptions:{

      activeTintColor:'#0086f8',
      inactiveTintColor:'#eee',
      labelStyle:{
        fontSize:10,
      },
      style:{
        backgroundColor:'#ffffff',
      },
      indicatorStyle:{
        borderBottomWidth:2,
        borderColor:'#0086f8',
      },
      showIcon:true,
      showLabel:false,
    },

}

);

export default NavigationTabs;
