import {Actions} from 'react-native-router-flux'
import React, { Component } from 'react';
import { Footer ,Button,Icon,Drawer} from 'native-base';

import {
  Platform,
  StyleSheet,
  Text,
  Dimensions,
  TextInput,
  Image,
  TouchableOpacity,
  ListView,

  View
} from 'react-native';

import data from '../../demodata/groupData/'
import chatScreen from './ChatScreen/'

class GroupDataScreen extends Component{
  constructor(props){
   super(props)
     const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
     this.state={
       dataSource:ds.cloneWithRows(groupData),
       text:''
     }
  }


  renderRow(rowData){

        const image=rowData.img;

        return(


                    <TouchableOpacity style={styles.singleContainer}
                                      onPress={()=>Actions.chatScreen()}>
                          <View style={styles.img}>
                                <Image style={styles.imgStyle}
                                source={require('../../assests/images/groupIcon.png')}
                                />
                          </View>
                          <View style={styles.Profile}>
                                <Text style={styles.pName}>{rowData.name}</Text>
                                <Text style={styles.msg}>{rowData.msg}</Text>
                          </View>
                          <View style={styles.timeHere}>
                          </View>
                          <Text style={styles.txttime}>{rowData.time}</Text>
                    </TouchableOpacity>

        )
  }
  render(){
    return(



      <ListView
        style={styles.mainContainer}
        enableEmptySections={true}
        renderRow={this.renderRow.bind(this)}
        dataSource={this.state.dataSource}
      />

    );
  }
}

const styles=StyleSheet.create({


  mainContainer:{

    marginBottom:150,
    backgroundColor:'#e2e5f2',
    padding:10,


  },

  singleContainer:{
    borderRadius:3,
    backgroundColor:'#ffffff',
    padding:10,
    marginBottom:5,
    flexDirection:'row',

  },
  img:{
    padding:5,
    borderRadius:100,
    justifyContent:'center',
  },
  imgStyle:{
     width:Platform.OS === 'ios'?50:40,
    height:Platform.OS === 'ios'?50:40,
    borderRadius:Platform.OS === 'ios'?25:50
  },
  Profile:{
    width:'70%',
    padding:5,
  },
  pName:{
    fontSize:14,
    fontWeight:'bold'
  },
  timeIcon:{
    fontSize:12
  },
  msg:{
    fontSize:13,
    color:'#a1adbc'
  },
});



export default GroupDataScreen;
