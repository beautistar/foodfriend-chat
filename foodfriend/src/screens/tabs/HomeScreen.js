
import React, { Component } from 'react';
import { Footer ,Button,Icon,Drawer} from 'native-base';

import {
  StyleSheet,
  Text,
  Dimensions,
  TextInput,
  Image,
  ImageBackground,
  ListView,
  TouchableOpacity,
  View
} from 'react-native';
import {DrawerNavigator} from 'react-navigation';

import HomeScreenSlider from '../sliderscreens/HomeScreenSlider/'
import DrawerLayoutOne from '../sliderscreens/DrawerLayoutOne/'
import data from '../../demodata/data/'

class HomeScreen extends Component{
  constructor(props){
   super(props)
     const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
     this.state={
       dataSource:ds.cloneWithRows(data),
       text:''
     }
 }


    renderRow(rowData){

          const image=rowData.img;

          return(


                      <View style={styles.singleContainer}>
                            <View style={styles.img}>
                                  <Image style={{width:50,height:50,borderRadius:50}}
                                  source={require('../../assests/images/user1.png')}
                                  />
                            </View>
                            <View style={styles.Profile}>
                                  <Text style={styles.pName}>{rowData.name}</Text>
                                  <Text style={styles.msg}>{rowData.msg}</Text>
                            </View>
                            <View style={styles.timeHere}>
                              <View style={{flexDirection:'row',}}>
                              <Icon name='time' style={{fontSize:12,marginTop:5,marginRight:3}}/>
                              <Text style={styles.txttime}>{rowData.time}</Text>
                              </View>
                            </View>

                      </View>

          )
    }

    filterSearch(text){
         const newData = data.filter(function(item){
             const itemData = item.name.toUpperCase()
             const textData = text.toUpperCase()
             return itemData.indexOf(textData) > -1
         })
         this.setState({
             dataSource: this.state.dataSource.cloneWithRows(newData),
             text: text
         })
     }
  render(){
    return(
      <View style={{flex:1,height:'100%'}}>

          <ImageBackground style={styles.topBackground}
              source={require('../../assests/images/topBar.png')} >
                <View>
                    <TouchableOpacity>
                        <Icon name='menu' size={20} style={{color:'#ffffff'}}/>
                    </TouchableOpacity>
                </View>
                <View style={styles.menuContainer}>

                          <Text style={styles.privateMessageStyle}>All Messages</Text>

                </View>
                <View>
                <TouchableOpacity>
                  <Image source={require('../../assests/images/create.png')} style={{height:30,width:30}}/>
                </TouchableOpacity>

                </View>


          </ImageBackground>

          <View style={{borderBottomWidth:0.5,backgroundColor:'#e4e7f4',}} >
                <TouchableOpacity style={styles.searchBox} onPress={()=>this.refs.searchBox.focus()}>

                      <View>
                            <Icon name='search' size={20} style={{color:'#e4e7f4',fontSize:25,}}/>
                      </View>
                      <View>
                            <TextInput
                              ref='searchBox'
                              underlineColorAndroid='transparent'
                              placeholder='Search'
                              onChangeText={(text)=>this.filterSearch(text)}
                              style={styles.textSearchStyle}/>
                      </View>
                </TouchableOpacity>
          </View>


            <ListView
              style={styles.mainContainer}
              enableEmptySections={true}
              renderRow={this.renderRow.bind(this)}
              dataSource={this.state.dataSource}
            />
      </View>
    );
  }
}

const styles=StyleSheet.create({

  topBackground:{

    height:50,
    paddingTop:20,
    backgroundColor:'#0880f3',
    flexDirection:'row',
    justifyContent:'space-between',

    padding:10,
  },
  menuContainer:{
    justifyContent:'center',
    alignItems:'center',
    flexDirection:'row'
  },
  privateMessageStyle:{
    fontSize:15,
    color:'#ffffff'
  },
  searchBox:{
    paddingLeft:10,
    margin:10,
    backgroundColor:'#ffffff',
    borderRadius:5,
    height:30,
    justifyContent:'center',
    alignItems:'center',
    flexDirection:'row',
  },
  viewText:{
    flex:3,
    justifyContent:'center',
    alignItems:'center',
    height:30,
    width:50,

  },
  textSearchStyle:{
    justifyContent:'center',
    alignItems:'center',
    fontSize:14,
    width:100,
    color:'#e4e7f4',
    alignItems:'center',
    height:40,
  },
  mainContainer:{
    flex:1,
    marginBottom:50,
    backgroundColor:'#e2e5f2',
    padding:10,

  },

  singleContainer:{
    borderRadius:3,
    backgroundColor:'#ffffff',
    padding:10,
    marginBottom:5,
    flexDirection:'row',

  },
  img:{
    padding:5,
    borderRadius:100,
    justifyContent:'center',
  },
  imgStyle:{
    width:40,
    height:40,
    borderRadius:50
  },
  Profile:{
    width:'70%',
    padding:5,
  },
  pName:{
    fontSize:14,
    fontWeight:'bold'
  },
  timeIcon:{
    fontSize:12
  },
  msg:{
    color:'#a1adbc'
  },
});


export default HomeScreen;
