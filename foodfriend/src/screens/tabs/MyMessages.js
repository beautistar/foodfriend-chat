import React, { Component } from 'react';
import { Header, Left, Body, Right,InputGroup,Icon,Input,Content} from 'native-base';
import {
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  TextInput,
  Image,
  ScrollView,
  View
} from 'react-native';

class MyMessages extends Component{

  render(){
    return(

      <ScrollView style={styles.mainContainer}>

          <View style={styles.singleContainer}>
                <View style={styles.img}>
                      <Image style={{width:50,height:50,borderRadius:50}}
                      source={require('../../assests/images/splash.png')}
                      />
                </View>
                <View style={styles.Profile}>
                      <Text style={styles.pName}>Profile Name</Text>
                      <Text style={styles.msg}>Message are going to here,,this text is for test</Text>
                </View>
                <View style={styles.timeHere}>
                </View>
                <Text style={styles.txttime}>10 m</Text>
          </View>
          <View style={styles.singleContainer}>
                <View style={styles.img}>
                      <Image style={{width:50,height:50,borderRadius:50}}
                      source={require('../../assests/images/splash.png')}
                      />
                </View>
                <View style={styles.Profile}>
                      <Text style={styles.pName}>Profile Name</Text>
                      <Text style={styles.msg}>Message are going to here,,this text is for test</Text>
                </View>
                <View style={styles.timeHere}>
                      <Text style={styles.txttime}>10 m</Text>
                </View>
          </View>
          <View style={styles.singleContainer}>
                <View style={styles.img}>
                      <Image style={{width:50,height:50,borderRadius:50}}
                      source={require('../../assests/images/splash.png')}
                      />
                </View>
                <View style={styles.Profile}>
                      <Text style={styles.pName}>Profile Name</Text>
                      <Text style={styles.msg}>Message are going to here,,this text is for test</Text>
                </View>
                <View style={styles.timeHere}>
                      <Text style={styles.txttime}>10 m</Text>
                </View>
          </View>



      </ScrollView>
    )
  }
}
const styles=StyleSheet.create({

  mainContainer:{
    backgroundColor:'#e2e5f2',
    padding:10
  },

  singleContainer:{
    borderRadius:3,
    backgroundColor:'#ffffff',
    padding:10,
    marginBottom:5,
    flexDirection:'row',

  },
  img:{
    padding:5,
    borderRadius:100,
    justifyContent:'center',


  },
  Profile:{
    width:'70%',
    padding:5,
  },
  pName:{
    fontSize:15,
    fontWeight:'bold'
  },

});
export default MyMessages;
