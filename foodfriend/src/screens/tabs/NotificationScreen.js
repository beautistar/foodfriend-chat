
import React, { Component } from 'react';
import { Footer ,Button,Icon,Drawer} from 'native-base';

import {
  Platform,
  StyleSheet,
  Text,
  ListView,
  Dimensions,
  TextInput,
  Image,
  ImageBackground,
  TouchableOpacity,
  View
} from 'react-native';

import data from '../../demodata/data/'

class NotificationScreen extends Component{
  static navigationOptions = {
      drawerLabel: 'Notifications',
      drawerIcon: ({ tintColor }) => (
        <Icon name='home' style={{}}/>
      ),
    };

  constructor(props){
   super(props)
     const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
     this.state={
       dataSource:ds.cloneWithRows(data),
       text:''
     }
 }


    renderRow(rowData){

          const image=rowData.img;

          return(


                      <View style={styles.singleContainer}>
                            <View style={styles.img}>
                                  <Image style={styles.imgStyle}
                                  source={require('../../assests/images/user1.png')}
                                  />
                            </View>
                            <View style={styles.Profile}>
                                  <Text style={styles.pName}>{rowData.name}</Text>
                                  <Text style={styles.msg}>{rowData.msg}</Text>
                            </View>
                            <View style={styles.timeHere}>
                                <View style={{flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                                <Icon name='time' style={{fontSize:12,marginRight:3,color:'#78909C',}}/>
                                <Text style={styles.txttime}>{rowData.time}</Text>
                                </View>
                            </View>

                      </View>

          )
    }

    filterSearch(text){
         const newData = data.filter(function(item){
             const itemData = item.name.toUpperCase()
             const textData = text.toUpperCase()
             return itemData.indexOf(textData) > -1
         })
         this.setState({
             dataSource: this.state.dataSource.cloneWithRows(newData),
             text: text
         })
     }

  render(){
    return(
      <View>

          <ImageBackground style={styles.topBackground}
              source={require('../../assests/images/topBar.png')} >
                <View>

                </View>
                <View style={styles.menuContainer}>

                          <Text style={styles.privateMessageStyle}>Notifications</Text>

                </View>
                <View>

                </View>


          </ImageBackground>



          <ListView
            style={styles.mainContainer}
            enableEmptySections={true}
            renderRow={this.renderRow.bind(this)}
            dataSource={this.state.dataSource}
          />

      </View>
    );
  }
}

const styles=StyleSheet.create({

  topBackground:{

    height:50,
    paddingTop:10,
    backgroundColor:'#0880f3',
    flexDirection:'row',
    justifyContent:'space-between',
    padding:10,
  },
  menuContainer:{
    justifyContent:'center',
    alignItems:'center',
    flexDirection:'row'
  },
  privateMessageStyle:{
    fontSize:15,
    color:'#ffffff'
  },

  mainContainer:{
    height:'90%',
    marginBottom:'10%',
    flexGrow: 1,
    backgroundColor:'#e2e5f2',
    padding:10,
    marginBottom:Platform.OS === 'ios' ?'15%':'10%',

  },

  singleContainer:{
    borderRadius:3,
    backgroundColor:'#ffffff',
    padding:10,
    marginBottom:5,
    flexDirection:'row',

  },
  img:{
    padding:5,
    borderRadius:100,
    justifyContent:'center',
  },
  imgStyle:{
    width:Platform.OS === 'ios'?50:40,
    height:Platform.OS === 'ios'?50:40,
    borderRadius:Platform.OS === 'ios'?25:50
  },
  Profile:{
    width:'70%',
    padding:5,
  },
  pName:{
    fontSize:14,
    fontWeight:'bold'
  },
  timeIcon:{
    fontSize:12
  },
  msg:{
    fontSize:13,
    color:'#a1adbc'
  },
  txttime:{
      color:'#78909C',
  },
});



export default NotificationScreen;
