import { Actions } from 'react-native-router-flux';
import React, { Component } from 'react';
import {
  Platform,
  TouchableOpacity,
  Text,
  Image,
  ImageBackground,
  StyleSheet,
  View
} from 'react-native';
import DataToShow from './DataToShow'


class MessageScreen extends Component{
  static navigationOptions = {

      // Note: By default the icon is only shown on iOS. Search the showIcon option below.
      tabBarIcon: ({ tintColor }) => (
        <Image style={{height:45,width:45,}} source={require('../../assests/images/msg.png')} />
      ),

    };

    constructor(props){
        super(props)

        this.state={
          privateMessage:true,
          groupMessage:false,
        }
    }
    loadSreenOne(){

        if(this.state.privateMessage){

          return (
            < DataToShow/>
          )
        }else{

          return (
          < GroupDataScreen/>
          )
        }
    }

  render(){
    return(
    <View>
          <View>
                <ImageBackground style={styles.topBackground}
                    source={require('../../assests/images/topBar.png')} >
                      <View>
                            <TouchableOpacity onPress={()=> Actions.messageScreen()}>
                            <Image source={require('../../assests/images/create.png')} style={{height:30,width:30}}/>
                            </TouchableOpacity>
                      </View>
                      <View style={styles.menuContainer}>
                            <TouchableOpacity  onPress={()=> this.setState({groupMessage:false,privateMessage:true})} style={[styles.privateMessageContainer,this.state.privateMessage?styles.selectedMenu:null ]} >
                                <Text style={styles.privateMessageStyle}>Private Messages</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={()=> this.setState({groupMessage:true,privateMessage:false})} style={[styles.groupMessageContainer,this.state.groupMessage?styles.selectedMenu:null  ]}>
                                <Text style={styles.groupMessageStyle}>Group Messages</Text>
                            </TouchableOpacity>
                      </View>
                      <View>
                          <TouchableOpacity>
                            <Image source={require('../../assests/images/search.png')} style={{height:30,width:30}}/>
                            </TouchableOpacity>
                      </View>
                </ImageBackground>
          </View>


          <View>
                {this.loadSreenOne()}

          </View>
    </View>

    );
  }
}

const styles=StyleSheet.create({

  topBackground:{

    height:50,
    paddingTop:10,
    backgroundColor:'#0880f3',
    flexDirection:'row',
    justifyContent:'space-between',
    padding:10,
  },
  menuContainer:{

    justifyContent:'center',
    alignItems:'center',
    flexDirection:'row'
  },
  privateMessageContainer:{
    padding:Platform.OS === 'ios' ?'7%':'5%',
    paddingLeft:30,
    paddingRight:10,
    alignItems:'center',
    justifyContent:'center',
    borderWidth:0.5,
    borderColor:'#ffffff',
    borderTopLeftRadius:50,
    borderBottomLeftRadius:50
  },
  privateMessageStyle:{
    fontSize:12,
    color:'#ffffff'
  },
  groupMessageContainer:{
    padding:7,
    borderWidth:0.5,
    paddingRight:30,
    borderColor:'#ffffff',
    borderTopRightRadius:50,
    borderLeftWidth:0,
    borderBottomRightRadius:50,

  },
  groupMessageStyle:{
    fontSize:12,
    color:'#ffffff'
  },
  selectedMenu:{
    backgroundColor:'#7a9bc4',
  },
})



export default MessageScreen;
