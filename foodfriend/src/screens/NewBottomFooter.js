import { Actions } from 'react-native-router-flux';
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  Platform,
  ScrollView,
  Image,
  ImageBackground,
  TextInput,
  ListView,
  TouchableOpacity,
  TouchableHighlight,
  View,Animated,Dimensions,TouchableWithoutFeedback
} from 'react-native';
import {DrawerNavigator} from 'react-navigation';
import Tabs from 'react-native-tabs';
import {Icon} from 'native-base';

import HomeScreenSlider from './sliderscreens/HomeScreenSlider/'
import DrawerLayoutOne from './sliderscreens/DrawerLayoutOne/'
import NotificationScreen from './tabs/NotificationScreen';
import SchedualScreen from './tabs/SchedualScreen';
import ScreenThree from './tabs/ScreenThree';
import MessageScreen from './tabs/MessageScreen';
import ProfileScreen from './tabs/ProfileScreen';
import HomeScreen from './tabs/HomeScreen';

const homeIconActive=require('../assests/images/home.png')
const homeIconInactive=require('../assests/images/homeInactive.png')
const bellIconActive=require('../assests/images/alarmActive.png')
const bellIconInactive=require('../assests/images/alarm.gif')
const schedualIconActive=require('../assests/images/thirdImageActive.png')
const schedualIconInactive=require('../assests/images/thirdImage.png')
const messageIconActive=require('../assests/images/msgActive.png')
const messageIconInactive=require('../assests/images/msg.png')
const profileIconActive=require('../assests/images/profileActive.png')
const profileIconInactive=require('../assests/images/profile.png')

import data from '../demodata/formessageonly/'
import DataToShow from './tabs/DataToShow'
import GroupDataScreen from './tabs/GroupDataScreen'

class NewBottomFooter extends Component {
  constructor(props){
   super(props)
     const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
     this.state={
       dataSource:ds.cloneWithRows(data),
       page:'home',
       text:'',
       privateMessage:true,
       groupMessage:false,
       animSearchBox:new Animated.Value(0),
     }

 }

  openSingle(){
    Actions.chatScreen();
  }

  //for messages threads
  renderRow(rowData){

          const image=rowData.img;

          return(


                      <TouchableOpacity style={styles.singleContainer} onPress={()=>this.openSingle()}>
                            <View style={styles.img}>
                                  <Image style={styles.imgStyle}
                                  source={require('../assests/images/user1.png')}
                                  />
                            </View>
                            <View style={{flex:1,paddingBottom:'2%',marginLeft:'1%',borderBottomWidth:0.5,borderColor:'#eee',}}>
                              <View style={styles.profile}>
                                    <View>
                                    <Text style={styles.pName}>{rowData.name}</Text>
                                    </View>

                                    <View style={styles.timeHere}>
                                      <View style={{flexDirection:'row',alignItems:'center'}}>
                                        <Icon name='time' style={{fontSize:12,color:'#78909C'}}/>
                                        <Text style={styles.txttime}>{rowData.time}</Text>
                                      </View>
                                  </View>
                              </View>

                              <View style={styles.msgContainer}>
                              <Text style={styles.msg}>{rowData.msg}</Text>
                              </View>
                          </View>
                      </TouchableOpacity>

          )
    }

    filterSearch(text){
         const newData = data.filter(function(item){
             const itemData = item.name.toUpperCase()
             const textData = text.toUpperCase()
             return itemData.indexOf(textData) > -1
         })
         this.setState({
             dataSource: this.state.dataSource.cloneWithRows(newData),
             text: text
         })
     }
   loadSreenOne(){

       if(this.state.privateMessage){

         return (
           < DataToShow/>
         )
       }else{

         return (
           < GroupDataScreen/>
         )
       }
   }


   _onBlur(){
   Animated.timing(this.state.animSearchBox,{
      toValue:0,
      uration:200
    }).start()
   }

   _animSearch(){
    this.refs.searchBox.focus();
   Animated.timing(this.state.animSearchBox,{
                toValue:Dimensions.get('window').width-150,
                duration:200
    }).start()
   }

  _tabbedNavigation(){
    const animSearch = {paddingRight:this.state.animSearchBox}
    switch(this.state.page){
      case 'home':return(

        <View style={{flex:1,height:'100%'}}>

            <ImageBackground style={styles.topBackground}
                source={require('../assests/images/topBar.png')} >
                  <View>
                      <TouchableOpacity onPress={()=> this.props.navigation.navigate('DrawerOpen')}>
                          <Icon name='menu' size={20} style={{color:'#ffffff'}}/>
                      </TouchableOpacity>
                  </View>
                  <View style={styles.menuContainer}>

                            <Text style={styles.allMessageStyle}>Home</Text>

                  </View>
                  <View>

                  <View>
                      <Icon name='menu' size={20} style={{color:'transparent'}}/>
                  </View>

                  </View>


            </ImageBackground>

            <View style={{justifyContent:'center',alignItems:'center',flex:1}}>
              <Text>Home</Text>
            </View>
        </View>

      )
      break;
      case 'bell':return(
        <View style={{flex:1,height:'100%'}}>

            <ImageBackground style={styles.topBackground}
                source={require('../assests/images/topBar.png')} >
                  <View>
                      <View>
                          <Icon name='menu' size={20} style={{color:'transparent'}}/>
                      </View>
                  </View>
                  <View style={styles.menuContainer}>

                            <Text style={styles.allMessageStyle}>Jobs</Text>

                  </View>
                  <View>

                  <View>
                      <Icon name='menu' size={20} style={{color:'transparent'}}/>
                  </View>

                  </View>


            </ImageBackground>

            <View style={{justifyContent:'center',alignItems:'center',flex:1}}>
              <Text>Jobs</Text>
            </View>
        </View>
      )
      break;
      case  'schedual':return(
        <View style={{flex:1,height:'100%'}}>

            <ImageBackground style={styles.topBackground}
                source={require('../assests/images/topBar.png')} >
                  <View>
                      <View>
                          <Icon name='menu' size={20} style={{color:'transparent'}}/>
                      </View>
                  </View>
                  <View style={styles.menuContainer}>

                            <Text style={styles.allMessageStyle}>Schedual</Text>

                  </View>
                  <View>

                  <View>
                      <Icon name='menu' size={20} style={{color:'transparent'}}/>
                  </View>

                  </View>


            </ImageBackground>

            <View style={{justifyContent:'center',alignItems:'center',flex:1}}>
              <Text>Schedual</Text>
            </View>
        </View>
      )
      break;
      case 'message':return(
        <View style={{flex:1,height:'100%'}}>

            <ImageBackground style={styles.topBackground}
                source={require('../assests/images/topBar.png')} >
                  <View>
                      <View>
                          <Icon name='menu' size={20} style={{color:'transparent'}}/>
                      </View>
                  </View>
                  <View style={styles.menuContainer}>

                            <Text style={styles.allMessageStyle}>All Messages</Text>

                  </View>
                  <View>
                  <TouchableOpacity  onPress={()=>Actions.newMessageChanged()}>
                    <Image source={require('../assests/images/create.png')} style={{height:30,width:30}}/>
                  </TouchableOpacity>

                  </View>


            </ImageBackground>

            <View style={{borderBottomWidth:0.3,backgroundColor:'#e4e7f4',}} >
                  <TouchableWithoutFeedback style={styles.searchBox} onPress={()=>this._animSearch()}>
                        <Animated.View style={[styles.searchBox,animSearch,{justifyContent:'center'},]}>
                            <View>
                                  <Icon name='search' size={20} style={{color:'#e4e7f4',fontSize:25,}}/>
                            </View>
                            <View>
                                  <TextInput
                                    ref='searchBox'
                                    underlineColorAndroid='transparent'
                                    placeholder='Search'
                                    onFocus={()=>this._animSearch()}
                                    onBlur={()=>this._onBlur()}
                                    placeholderTextColor={'#a1adbc'}
                                    onChangeText={(text)=>this.filterSearch(text)}
                                    style={styles.textSearchStyle}/>
                            </View>
                        </Animated.View>
                  </TouchableWithoutFeedback>
            </View>


              <ListView
                style={styles.mainContainer}
                enableEmptySections={true}
                renderRow={this.renderRow.bind(this)}
                dataSource={this.state.dataSource}
              />
        </View>
                           )
      break;
      case 'profile':return(
        <View style={{flex:1,height:'100%'}}>

            <ImageBackground style={styles.topBackground}
                source={require('../assests/images/topBar.png')} >
                  <View>
                      <View>
                          <Icon name='menu' size={20} style={{color:'transparent'}}/>
                      </View>
                  </View>
                  <View style={styles.menuContainer}>

                            <Text style={styles.allMessageStyle}>Profile</Text>

                  </View>
                  <View>

                  <View>
                      <Icon name='menu' size={20} style={{color:'transparent'}}/>
                  </View>

                  </View>


            </ImageBackground>

            <View style={{justifyContent:'center',alignItems:'center',flex:1}}>
              <Text>Profile</Text>
            </View>
        </View>
      )
      break;
      default:
    }
  }

  render() {
    var self = this;
    return(

          <View style={styles.container}>
              <View style={{flex:1,}}>
              {this._tabbedNavigation()}
              </View>
              <View style={styles.footer}>
                  <Tabs selected={this.state.page} style={styles.tabContainer}
                    onSelect={el=>this.setState({page:el.props.name})}>

                    {this.state.page=='home'?<Image name='home' style={{height:40,width:40,}} source={homeIconActive} />
                                            :<Image name='home' style={{height:40,width:40,}} source={homeIconInactive} />}

                      {this.state.page=='bell'?<Image name='bell' style={{height:50,width:50,}}  source={bellIconActive}/>
                                              :<Image name='bell' style={{height:50,width:50,}} source={bellIconInactive} />}

                      {this.state.page=='schedual'?<Image name='schedual' style={{height:40,width:40,}} source={schedualIconActive}/>
                                                  :<Image name='schedual' style={{height:40,width:40,}} source={schedualIconInactive} />}

                      {this.state.page=='message'?<Image name='message' style={{height:40,width:40,}} source={messageIconActive}/>
                                                  :<Image name='message' style={{height:40,width:40,}} source={messageIconInactive} />}

                      {this.state.page=='profile'?<Image name='profile' style={{height:40,width:40,}} source={profileIconActive}/>
                                                :<Image name='profile' style={{height:40,width:40,}} source={profileIconInactive} />}
                    </Tabs>
              </View>
          </View>
    )
 }
}
const styles=StyleSheet.create({
  container: {
    flex: 1,
  },

  tabContainer:{
    borderTopWidth:0.4,
    borderTopColor:'#b0bec5',
    backgroundColor:'#ffffff',
    height:56,
  },
  topBackground:{

    height:50,
    paddingTop:Platform.OS === 'ios'?20:10,
    backgroundColor:'#0880f3',
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems:'center',
    padding:10,
  },
  menuContainer:{
    justifyContent:'center',
    alignItems:'center',
    flexDirection:'row'
  },
  allMessageStyle:{
    fontSize:15,
    color:'#ffffff'
  },

  privateMessageStyle:{
    fontSize:Platform.OS ==='ios'?11:12,
    color:'#ffffff'
  },
  searchBox:{
    paddingLeft:10,
    margin:10,
    backgroundColor:'#ffffff',
    borderRadius:5,
    height:30,
    alignItems:'center',
    flexDirection:'row',
  },
  viewText:{
    flex:3,
    justifyContent:'center',
    alignItems:'center',
    height:30,
    width:50,

  },
  textSearchStyle:{
    justifyContent:'center',
    alignItems:'center',
    fontSize:14,
    width:100,
    alignItems:'center',
    height:40,
  },
  mainContainer:{
    flex:1,
    marginBottom:50,
    backgroundColor:'#ffffff',
    padding:0,

  },

  singleContainer:{
    borderRadius:3,
    backgroundColor:'#ffffff',
    padding:10,
    marginBottom:0,
    flexDirection:'row',

  },
  img:{
    padding:5,
    borderRadius:100,
    justifyContent:'center',
  },
  imgStyle:{
    width:Platform.OS === 'ios'?50:40,
    height:Platform.OS === 'ios'?50:40,
    borderRadius:Platform.OS === 'ios'?25:50
  },
  profile:{

    flexDirection:'row',
    justifyContent:'space-between',
    paddingHorizontal:0,
  },
  msgContainer:{

  },
  pName:{
    fontSize:14,
    fontWeight:'bold'
  },
  timehere:{

    flexDirection:'row',
    fontSize:12
  },
  privateMessageContainer:{
    padding:Platform.OS ==='ios'?8:7,
    paddingLeft:Platform.OS ==='ios'?'4%':'5%',
    //paddingRight:10,
    alignItems:'center',
    justifyContent:'center',
    borderWidth:Platform.OS ==='ios'?1.5:1,
    borderColor:'##rgba(255,255,255,0.5)',
    borderRightWidth:0,
    borderTopLeftRadius:15,
    borderBottomLeftRadius:15
  },
  groupMessageContainer:{
    padding:Platform.OS ==='ios'?6:7,
    borderWidth:Platform.OS ==='ios'?1.5:1,
    paddingRight:Platform.OS ==='ios'?'4%':'5%',
    borderColor:'#rgba(255,255,255,0.5)',
    borderTopRightRadius:15,

    borderBottomRightRadius:15,

  },
  groupMessageStyle:{
    fontSize:Platform.OS ==='ios'?11:12,
    color:'#ffffff'
  },
  selectedMenu:{
    backgroundColor:'#7a9bc4',
  },
  txttime:{
    color:'#78909C',
    paddingHorizontal:'0.5%'
  },
  msg:{
    fontSize:13,
    color:'#a1adbc'
  },
});


 export default NewBottomFooter;
