import {Actions} from 'react-native-router-flux'
  import type {
  StyleObj,
} from 'react-native/Libraries/StyleSheet/StyleSheetTypes';

import React, { Component } from 'react';
import {
  Platform, StyleSheet,Text,
  FlatList,Dimensions,ScrollView,
  TouchableOpacity,KeyboardAvoidingView,
  TextInput,ImageBackground,Image,
  View,Keyboard,Animated,ListView,
  TouchableWithoutFeedback,

} from 'react-native';
//library for chip/tags
import TagInput from '../lib/TagInput';


import {TabNavigator} from 'react-navigation';
import {CheckBox, Container, Header, Tab,Icon, Tabs , ScrollableTab,FooterTab,Footer} from 'native-base';
import Restaurants from './messagetabs/Restaurants/'

import checkBoxListData from '../demodata/checkBoxListData'
import data from '../demodata/formessageonly/'
var selectedUser=[];
const rec=1;
class NewMessageChanged extends Component{

  constructor(props){
    super(props);

    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
    //this.refs.search.focus();

    this.state={
        tags: [],
        text: "",
        text1:'',
        to:'',
        newData:checkBoxListData,
        dataSource:ds.cloneWithRows(checkBoxListData),
        animSearchBox:new Animated.Value(-10),
        mHeight:510,
        end:false
    }

  }

  renderRow(rowData){

        return(

          <TouchableOpacity style={styles.singleContainer} onPress={()=>Actions.chatScreen()}>

                <View style={styles.img}>
                      <Image style={styles.imgStyle}
                      source={require('../assests/images/user3.png')}
                      />
                </View>
                <View style={styles.Profile}>
                      <Text style={styles.m}>{rowData.name}</Text>
                      <Text style={styles.msg}>{rowData.msg}</Text>
                </View>

          </TouchableOpacity>
        )
  }


  // For updatting the dataSource by searching
  filterSearch(text){
       const newData = checkBoxListData.filter(function(item){
           const itemData = item.name.toUpperCase()
           const textData = text.toUpperCase()
           return itemData.indexOf(textData) > -1
       })
       this.setState({
           dataSource: this.state.dataSource.cloneWithRows(newData),
           text1: text
       })
   }

   //for animatting the search box
   _onBlur(){
   Animated.timing(this.state.animSearchBox,{
      toValue:-10,
      uration:200
    }).start()
   }

   _animSearch(){
    this.refs.searchBox.focus();
   Animated.timing(this.state.animSearchBox,{
                toValue:Dimensions.get('window').width-150,
                duration:200
    }).start()
   }
//for inner scroll
   _MoveRight(){
     // this.setState({
     //   end:!this.state.end
     // })
     // this.state.end ?this.refs.innnerview.scrollTo({y:0,animated:true}):this.refs.innnerview.scrollToEnd()
     Actions.newMessage()


   }
  render(){
    const animSearch = {paddingRight:this.state.animSearchBox}
    const inputProps = {
         keyboardType: 'default',
         placeholder: '',
         autoFocus: true,
         style: {
           fontSize: 14,
           marginVertical: Platform.OS == 'ios' ? 10 : -2,
         },
       };
    return(

      <View  style={styles.container} >

            <ImageBackground style={styles.topBackground}
                source={require('../assests/images/topBar.png')} >
                  <View>
                        <TouchableOpacity onPress={()=> Actions.pop()} style={{height:40,width:60}}>
                          <Icon name='arrow-back' style={{color:'#ffffff',fontSize:25,}}/>
                        </TouchableOpacity>
                  </View>
                  <View style={styles.menuContainer}>

                            <Text style={styles.privateMessageStyle}>New Message</Text>

                  </View>
                  <View>

                  </View>


            </ImageBackground>

            <View style={styles.thirdCon} keyboardShouldPersistTabs='always'
                             keyboardDismissMode='on-drag'>

                   <Tabs  renderTabBar={()=> <ScrollableTab />} tabBarUnderlineStyle={{ backgroundColor:'#0086f8', }} >

                     <Tab heading="ALL" tabStyle={styles.tabBarBackColor} textStyle={styles.tabTextStyle} activeTabStyle={styles.activeTabStyle} activeTextStyle={styles.activeTextStyle}>

                        <View style={{marginBottom:0,backgroundColor:'#e4e7f4'}}>
                          <TouchableWithoutFeedback style={styles.searchBox} onPress={()=>this._animSearch()}>
                                <Animated.View style={[styles.searchBox,animSearch,{justifyContent:'center'},]}>
                                    <View>
                                          <Icon name='search' size={20} style={{color:'#a1adbc',fontSize:25,}}/>
                                    </View>
                                    <View>
                                          <TextInput
                                            ref='searchBox'
                                            underlineColorAndroid='transparent'
                                            placeholder='Search'
                                            onFocus={()=>this._animSearch()}
                                            onBlur={()=>this._onBlur()}
                                            placeholderTextColor={'#a1adbc'}
                                            onChangeText={(text)=>this.filterSearch(text)}
                                            style={styles.textSearchStyle}/>
                                    </View>
                                </Animated.View>
                          </TouchableWithoutFeedback>

                          <TouchableOpacity onPress={()=>this._MoveRight()}  style={styles.newMessageContainer}>
                            <Image style={styles.imgIcon} source={require('../assests/images/grp.png')}/>
                            <Text style={styles.newMessageText}>New Group Message</Text>

                              <View style={{height:50,width:50,alignItems:'center',justifyContent:'center'}}>
                               {this.state.end?<Icon name='arrow-back' style={styles.arrow}/>
                                              :<Icon name='arrow-forward' style={styles.arrow}/>}
                               </View>
                             </TouchableOpacity>

                           <ListView
                             style={[styles.scrollViewStyle]}
                             enableEmptySections={true}

                             renderRow={this.renderRow.bind(this)}
                             dataSource={this.state.dataSource}
                            />


                       </View>

                     </Tab>

                     <Tab heading="RESTAURANT" tabStyle={styles.tabBarBackColor} textStyle={styles.tabTextStyle} activeTabStyle={styles.activeTabStyle} activeTextStyle={styles.activeTextStyle}>
                       <Restaurants />
                     </Tab>

                     <Tab heading="TYPES" tabStyle={styles.tabBarBackColor} textStyle={styles.tabTextStyle} activeTabStyle={styles.activeTabStyle} activeTextStyle={styles.activeTextStyle}>

                     </Tab>

                     <Tab heading="GROUPS" tabStyle={styles.tabBarBackColor} textStyle={styles.tabTextStyle} activeTabStyle={styles.activeTabStyle} activeTextStyle={styles.activeTextStyle}>

                     </Tab>

                   </Tabs>
           </View>

      </View>

    );
  }
}
const styles=StyleSheet.create({
  topBackground:{

    height:50,
    paddingTop:Platform.OS === 'ios'?20:10,
    backgroundColor:'#0880f3',
    flexDirection:'row',
    justifyContent:'space-between',
    padding:10,
  },
  privateMessageStyle:{
    fontSize:15,
    marginTop:5,
    marginRight:'10%',
    color:'#ffffff'
  },

  container:{
    flex:1,
    backgroundColor:'#ffffff',

    //justifyContent:'space-between',
  },
  firstCon:{
    zIndex:-1,
    justifyContent:'center',
    alignItems:'center',
    flexDirection:'row',
    borderBottomWidth:0.3,
    borderColor:'#ffffff',

  },
  imgBox:{
    backgroundColor:'#ffffff',
    height:50,
    width:50,
  },
  inputBox:{
    paddingLeft:5,
    borderWidth:0,
    backgroundColor:'#ffffff',
    width:'60%',
    height:40,
  },
  btnStyle:{
    justifyContent:'center',
    alignItems:'center',
    backgroundColor:'#2d87d9',
    width:'20%',
    height:35,
    padding:10,
  },
  btnTextStyle:{
    color:'#ffffff',
  },
  secondCon:{
    height:Platform.OS ==='ios'?40:40,
    backgroundColor:'#ffffff',
    flexDirection:'row',
    alignItems:'center',
    paddingHorizontal:10,
    borderBottomWidth:0.3,
    borderColor:'black',
  },
  toTextStyle:{
    color:'#2d87d9',
    fontWeight:'bold',
  },
  thirdCon:{
   // height:Platform.OS ==='ios'?735:570,
    flex:1,
    borderBottomWidth:1,
    borderColor:'black',
    backgroundColor:'#ffffff',

  },
  tabBarBackColor:{
    backgroundColor:'#ffffff',

  },
  activeTabStyle:{
    backgroundColor:'#ffffff'
  },
  activeTextStyle:{
    color: 'black',
    fontSize:15,
  },
  tabTextStyle:{
    color:'#b1b5be',
    fontSize:15,
  },
  searchBox:{
    paddingLeft:10,
    margin:10,
    backgroundColor:'#ffffff',
    borderRadius:5,
    height:30,
    alignItems:'center',
    flexDirection:'row',
  },
  viewText:{
    flex:3,
    justifyContent:'center',
    alignItems:'flex-start',
    height:30,
    width:50
  },
  textSearchStyle:{
    justifyContent:'center',
    alignItems:'center',
    fontSize:14,
    width:100,
    alignItems:'center',
    height:40,
  },
  scrollViewStyle:{
    backgroundColor:'#ffffff',
    paddingTop:10,
   //maxHeight:Platform.OS ==='ios'?500:340,
   paddingBottom:100,
   width:Dimensions.get('window').width,

  },
  mainView:{
    backgroundColor:'#e4e7f4',

  },
  singleContainer:{
    borderRadius:3,
    backgroundColor:'#ffffff',
    paddingLeft:10,
    flexDirection:'row',

  },
  img:{
    padding:5,
    borderRadius:100,
    justifyContent:'center',
  },
  imgStyle:{
    width:Platform.OS === 'ios'?50:40,
    height:Platform.OS === 'ios'?50:40,
    borderRadius:Platform.OS === 'ios'?25:50
  },
  Profile:{
    width:'70%',
    flex:1,
    padding:5,
    borderBottomWidth:0.5,
    borderBottomColor:'#eee',
  },
  m:{

    fontSize:14,
    fontWeight:'bold'
  },

  msg:{
    //color:'#d2d2d2',
    color:'#a1adbc',
    fontSize:13,
  },
  newMessageContainer:{
    flexDirection:'row',
    backgroundColor:'#ffffff',
    height:50,
    paddingLeft:'3%',
    justifyContent:'space-between',
    alignItems:'center',
    borderBottomWidth:0.5,
    borderBottomColor:'#eee'
  },
  iconStyle:{
    paddingRight:Platform.OS === 'ios'?'8%':'3%',
    paddingLeft:Platform.OS === 'ios'?'4%':'1%',
  },
  pName:{
    fontSize:14,
    fontWeight:'bold'
  },
  newMessageText:{
    flex:2,
    //color:'#d2d2d2',
    color:'#a1adbc'
  },
  imgIcon:{
    height:Platform.OS ==='ios'?'60%':'50%',
    width:Platform.OS ==='ios'?'7%':'10%'
  },
  arrow:{
    color:'lightgrey',
    fontSize:18,
  },
})
export default NewMessageChanged;
