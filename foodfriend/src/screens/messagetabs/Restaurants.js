
import React, { Component } from 'react';
import { Footer ,Button,Icon,Drawer} from 'native-base';

import {
  Platform,
  StyleSheet,
  Text,
  Dimensions,
  TextInput,
  Image,
  ImageBackground,
  ScrollView,
  TouchableOpacity,
  View,Animated,TouchableWithoutFeedback
} from 'react-native';


const imgRestaurant=require('../../assests/images/restaurant.png')
class Restaurants extends Component{
  constructor(props){
    super(props);

    this.state={

        animSearchBox:new Animated.Value(-10),
    }

  }
  static navigationOptions = {

      // Note: By default the icon is only shown on iOS. Search the showIcon option below.
      tabBarIcon: ({ tintColor }) => (
        <Image style={{height:45,width:45,}} source={require('../../assests/images/home.png')} />
      ),

    };

   closeDrawer = () => {
     this.drawer._root.close()
   };
   openDrawer = () => {
     this.drawer._root.open()
   };

   _onBlur(){
   Animated.timing(this.state.animSearchBox,{
      toValue:-10,
      uration:200
    }).start()
   }

   _animSearch(){
    this.refs.searchBox.focus();
   Animated.timing(this.state.animSearchBox,{
                toValue:Dimensions.get('window').width-150,
                duration:200
    }).start()
   }
  render(){
    const animSearch = {paddingRight:this.state.animSearchBox}
    return(
      <View style={{}}>

          <View style={styles.serchBoxContainer}>
            <TouchableWithoutFeedback style={styles.searchBox} onPress={()=>this._animSearch()}>
                  <Animated.View style={[styles.searchBox,animSearch,{justifyContent:'center'},]}>
                      <View>
                            <Icon name='search' size={20} style={{color:'#e4e7f4',fontSize:25,}}/>
                      </View>
                      <View>
                            <TextInput
                              ref='searchBox'
                              underlineColorAndroid='transparent'
                              placeholder='Search'
                              onFocus={()=>this._animSearch()}
                              onBlur={()=>this._onBlur()}
                              placeholderTextColor={'#dadada'}
                              onChangeText={(text)=>this.filterSearch(text)}
                              style={styles.textSearchStyle}/>
                      </View>
                  </Animated.View>
            </TouchableWithoutFeedback>
          </View>


          <ScrollView style={styles.mainContainer}>

              <View style={styles.singleContainer}>
                    <View style={styles.img}>
                          <Image style={styles.imgStyle}
                          source={imgRestaurant}
                          />
                    </View>
                    <View style={styles.Profile}>
                          <Text style={styles.pName}>KFC</Text>
                          <Text style={styles.msg}>city</Text>
                    </View>
                    <View style={styles.timeHere}>
                    </View>

              </View>
              <View style={styles.singleContainer}>
                    <View style={styles.img}>
                          <Image style={styles.imgStyle}
                          source={imgRestaurant}
                          />
                    </View>
                    <View style={styles.Profile}>
                          <Text style={styles.pName}>JainCafe</Text>
                          <Text style={styles.msg}>city</Text>
                    </View>
                    <View style={styles.timeHere}>
                    </View>

              </View>
              <View style={styles.singleContainer}>
                    <View style={styles.img}>
                          <Image style={styles.imgStyle}
                          source={imgRestaurant}
                          />
                    </View>
                    <View style={styles.Profile}>
                          <Text style={styles.pName}>Calorine</Text>
                          <Text style={styles.msg}>city</Text>
                    </View>
                    <View style={styles.timeHere}>
                    </View>

              </View>

              <View style={styles.singleContainer}>
                    <View style={styles.img}>
                          <Image style={styles.imgStyle}
                          source={imgRestaurant}
                          />
                    </View>
                    <View style={styles.Profile}>
                          <Text style={styles.pName}>Classic Touch</Text>
                          <Text style={styles.msg}>city</Text>
                    </View>
                    <View style={styles.timeHere}>
                    </View>

              </View>

              <View style={styles.singleContainer}>
                    <View style={styles.img}>
                          <Image style={styles.imgStyle}
                          source={imgRestaurant}
                          />
                    </View>
                    <View style={styles.Profile}>
                          <Text style={styles.pName}>Luicy Spot</Text>
                          <Text style={styles.msg}>city</Text>
                    </View>
                    <View style={styles.timeHere}>
                    </View>

              </View>

              <View style={styles.singleContainer}>
                    <View style={styles.img}>
                          <Image style={styles.imgStyle}
                          source={imgRestaurant}
                          />
                    </View>
                    <View style={styles.Profile}>
                          <Text style={styles.pName}>Points</Text>
                          <Text style={styles.msg}>city</Text>
                    </View>
                    <View style={styles.timeHere}>
                    </View>

              </View>
          </ScrollView>

      </View>
    );
  }
}

const styles=StyleSheet.create({

  topBackground:{

    height:70,
    paddingTop:30,
    backgroundColor:'#0880f3',
    flexDirection:'row',
    justifyContent:'space-between',
    padding:10,
  },
  menuContainer:{
    justifyContent:'center',
    alignItems:'center',
    flexDirection:'row'
  },
  privateMessageStyle:{
    fontSize:15,
    color:'#ffffff'
  },
  searchBox:{
    paddingLeft:10,
    margin:10,
    backgroundColor:'#ffffff',
    borderRadius:5,
    height:30,
    justifyContent:'center',
    alignItems:'center',
    flexDirection:'row',
  },
  viewText:{
    flex:3,
    justifyContent:'center',
    alignItems:'center',
    height:30,
    width:50,

  },
  textSearchStyle:{
    justifyContent:'center',
    alignItems:'center',
    fontSize:14,
    width:100,
    alignItems:'center',
    height:40,
  },
  mainContainer:{
    marginBottom:Platform.OS === 'ios'?0:50,
  //  backgroundColor:'#e2e5f2',
  },

  singleContainer:{
    backgroundColor:'#ffffff',
    padding:10,
    flexDirection:'row',

  },
  img:{
    padding:5,
  //  borderRadius:100,
    justifyContent:'center',
  },
  imgStyle:{
    width:Platform.OS === 'ios'?50:40,
    height:Platform.OS === 'ios'?50:40,
    borderRadius:Platform.OS === 'ios'?25:50
  },
  Profile:{
    width:Dimensions.get('window').width,
    padding:5,
    borderBottomColor:'#eee',
    borderBottomWidth:1,
  },
  pName:{
    fontSize:14,
    fontWeight:'bold'
  },
  timeIcon:{
    fontSize:12
  },
  serchBoxContainer:{

    backgroundColor:'#e4e7f4',
  },
  msg:{
    //color:'#d2d2d2',
      color:'#a1adbc',
    fontSize:13,
  },
});



export default Restaurants;
