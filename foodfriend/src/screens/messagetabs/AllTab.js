
import React, { Component } from 'react';
import { Footer ,Button,Icon,Drawer} from 'native-base';

import {
  Platform,
  StyleSheet,
  Text,
  Dimensions,
  TextInput,
  Image,
  ImageBackground,
  ScrollView,
  TouchableOpacity,
  View
} from 'react-native';



class AllTab extends Component{
  static navigationOptions = {

      // Note: By default the icon is only shown on iOS. Search the showIcon option below.
      tabBarIcon: ({ tintColor }) => (
        <Image style={{height:45,width:45,}} source={require('../../assests/images/home.png')} />
      ),

    };

   closeDrawer = () => {
     this.drawer._root.close()
   };
   openDrawer = () => {
     this.drawer._root.open()
   };

  render(){
    return(
      <View>

          <View style={styles.serchBoxContainer}>
                <TouchableOpacity style={styles.searchBox} onPress={()=>this.refs.searchInput.focus()}>

                      <View>
                            <Icon name='search' size={20} style={{color:'#e4e7f4',fontSize:25,}}/>
                      </View>
                      <View>
                            <TextInput
                              ref='searchInput'
                              underlineColorAndroid='transparent'
                              placeholder='Search'
                              placeholderColor={'#b0b5c7'}
                              style={styles.textSearchStyle}/>
                      </View>
                </TouchableOpacity>
          </View>


          <ScrollView style={styles.mainContainer}>

              <View style={styles.singleContainer}>
                    <View style={styles.img}>
                          <Image style={styles.imgStyle}
                          source={require('../../assests/images/user1.png')}
                          />
                    </View>
                    <View style={styles.Profile}>
                          <Text style={styles.pName}>Lorein Ops</Text>
                          <Text style={styles.msg}>Message are going to here,,this text is for test</Text>
                    </View>
                    <View style={styles.timeHere}>
                    </View>
                    <Text style={styles.txttime}><Icon name='time' size={3} style={styles.timeIcon} />10 am</Text>
              </View>
              <View style={styles.singleContainer}>
                    <View style={styles.img}>
                          <Image style={styles.imgStyle}
                          source={require('../../assests/images/user2.png')}
                          />
                    </View>
                    <View style={styles.Profile}>
                          <Text style={styles.pName}>Alex Jain</Text>
                          <Text style={styles.msg}>Message are going to here,,this text is for test</Text>
                    </View>
                    <View style={styles.timeHere}>
                    </View>
                    <Text style={styles.txttime}><Icon name='time' size={3} style={styles.timeIcon} />10 am</Text>
              </View>
              <View style={styles.singleContainer}>
                    <View style={styles.img}>
                          <Image style={styles.imgStyle}
                          source={require('../../assests/images/user3.png')}
                          />
                    </View>
                    <View style={styles.Profile}>
                          <Text style={styles.pName}>Huge Denial</Text>
                          <Text style={styles.msg}>Message are going to here,,this text is for test</Text>
                    </View>
                    <View style={styles.timeHere}>
                    </View>
                    <Text style={styles.txttime}><Icon name='time' size={3} style={styles.timeIcon} />10 am</Text>
              </View>

              <View style={styles.singleContainer}>
                    <View style={styles.img}>
                          <Image style={styles.imgStyle}
                          source={require('../../assests/images/user3.png')}
                          />
                    </View>
                    <View style={styles.Profile}>
                          <Text style={styles.pName}>Huge Denial</Text>
                          <Text style={styles.msg}>Message are going to here,,this text is for test</Text>
                    </View>
                    <View style={styles.timeHere}>
                    </View>
                    <Text style={styles.txttime}><Icon name='time' size={3} style={styles.timeIcon} />10 am</Text>
              </View>

              <View style={styles.singleContainer}>
                    <View style={styles.img}>
                          <Image style={styles.imgStyle}
                          source={require('../../assests/images/user3.png')}
                          />
                    </View>
                    <View style={styles.Profile}>
                          <Text style={styles.pName}>Huge Denial</Text>
                          <Text style={styles.msg}>Message are going to here,,this text is for test</Text>
                    </View>
                    <View style={styles.timeHere}>
                    </View>
                    <Text style={styles.txttime}><Icon name='time' size={3} style={styles.timeIcon} />10 am</Text>
              </View>

              <View style={styles.singleContainer}>
                    <View style={styles.img}>
                          <Image style={styles.imgStyle}
                          source={require('../../assests/images/user3.png')}
                          />
                    </View>
                    <View style={styles.Profile}>
                          <Text style={styles.pName}>Huge Denial</Text>
                          <Text style={styles.msg}>Message are going to here,,this text is for test</Text>
                    </View>
                    <View style={styles.timeHere}>
                    </View>
                    <Text style={styles.txttime}><Icon name='time' size={3} style={styles.timeIcon} />10 am</Text>
              </View>
          </ScrollView>

      </View>
    );
  }
}

const styles=StyleSheet.create({

  topBackground:{

    height:70,
    paddingTop:30,
    backgroundColor:'#0880f3',
    flexDirection:'row',
    justifyContent:'space-between',
    padding:10,
  },
  menuContainer:{
    justifyContent:'center',
    alignItems:'center',
    flexDirection:'row'
  },
  privateMessageStyle:{
    fontSize:15,
    color:'#ffffff'
  },
  searchBox:{
    paddingLeft:10,
    margin:10,
    backgroundColor:'#ffffff',
    borderRadius:5,
    height:30,
    justifyContent:'center',
    alignItems:'center',
    flexDirection:'row',
  },
  viewText:{
    flex:3,
    justifyContent:'center',
    alignItems:'center',
    height:30,
    width:50,

  },
  textSearchStyle:{
    justifyContent:'center',
    alignItems:'center',
    fontSize:14,
    width:100,
    alignItems:'center',
    height:40,
  },
  mainContainer:{
    maxHeight:500,
    backgroundColor:'#e2e5f2',
    padding:10,
    marginBottom:100,

  },

  singleContainer:{
    borderRadius:3,
    backgroundColor:'#FAFAFA',
    padding:10,
    marginBottom:5,
    flexDirection:'row',

  },
  img:{
    padding:5,
    borderRadius:100,
    justifyContent:'center',
  },
  imgStyle:{
    width:Platform.OS === 'ios'?50:40,
    height:Platform.OS === 'ios'?50:40,
    borderRadius:Platform.OS === 'ios'?25:50
  },
  Profile:{
    width:'70%',
    padding:5,
  },
  pName:{
    fontSize:14,
    fontWeight:'bold'
  },
  timeIcon:{
    fontSize:12
  },
  serchBoxContainer:{
    borderBottomWidth:0.5,
    backgroundColor:'#e4e7f4',
  },
});



export default AllTab;
