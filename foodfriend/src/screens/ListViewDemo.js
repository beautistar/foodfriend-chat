
import React, {Component} from 'react'
import {
    Text,
    StyleSheet,
    View,
    ListView,
    TouchableHighlight,
    Dimensions,
    Image,
    Animated,
    TextInput
} from 'react-native'
import {DrawerNavigator} from 'react-navigation';


import HomeScreenSlider from './sliderscreens/HomeScreenSlider/'
import DrawerLayoutOne from './sliderscreens/DrawerLayoutOne/'

import data from '../demodata/data/'

class ListViewDemo extends Component{
  constructor(props){
   super(props)
     const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
     this.state={
       dataSource:ds.cloneWithRows(data),
       text:''
     }
 }

  renderRow(rowData){

        const image=rowData.img;

        return(


                    <View style={styles.singleContainer}>
                          <View style={styles.img}>
                                <Image style={{width:50,height:50,borderRadius:50}}
                                source={require('../assests/images/user1.png')}
                                />
                          </View>
                          <View style={styles.Profile}>
                                <Text style={styles.pName}>{rowData.name}</Text>
                                <Text style={styles.msg}>{rowData.msg}</Text>
                          </View>
                          <View style={styles.timeHere}>
                          </View>
                          <Text style={styles.txttime}>{rowData.time}</Text>
                    </View>

        )
  }
  filterSearch(text){
       const newData = data.filter(function(item){
           const itemData = item.name.toUpperCase()
           const textData = text.toUpperCase()
           return itemData.indexOf(textData) > -1
       })
       this.setState({
           dataSource: this.state.dataSource.cloneWithRows(newData),
           text: text
       })
   }
  render(){

    return(

      <View style={{padding:10,backgroundColor:'lightblue',flex:1,}}>
          <View>
            <TextInput placeholder='Search Name' onChangeText={(text)=>this.filterSearch(text)}/>
          </View>
            <View>
                  <ListView
                    style={{minHeight:'80%',marginBottom:50}}
                    enableEmptySections={true}
                    renderRow={this.renderRow.bind(this)}
                    dataSource={this.state.dataSource}
                  />

            </View>
      </View>


    )
  }
}
const styles=StyleSheet.create({

  mainContainer:{
    backgroundColor:'#e2e5f2',
    padding:10
  },

  singleContainer:{
    borderRadius:3,
    backgroundColor:'#ffffff',
    padding:10,
    marginBottom:5,
    flexDirection:'row',

  },
  img:{
    padding:5,
    borderRadius:100,
    justifyContent:'center',


  },
  Profile:{
    width:'70%',
    padding:5,
  },
  pName:{
    fontSize:15,
    fontWeight:'bold'
  },

});

export default ListViewDemo;
